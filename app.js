var app = require('express')();
var mongoose = require('mongoose');
var db = mongoose.connection;
var http = require('http').Server(app);
var io = require('socket.io')(http);
var assert = require('assert');
var Client = require('node-rest-client').Client;
var client = new Client();
var urls = "http://api.rajneeti.dev/v1";
var user_id = {};
// messages store database
var chatSchema = mongoose.Schema({
    message: String,
    sender: String,
    receiver: String,
    created: {type: Date, default: Date.now}
});

var Chat = mongoose.model('Message', chatSchema);
	module.exports = Chat;
// messages store database end

// socket.io start
io.on('connection', function(socket){
	// online user request send
	socket.on('userStatus', function(res){
		user_id = res.id;
		client.get(res.url, function (data, response) {
	    	// parsed response body as js object 
	    	
		});	
	})
	
 	socket.on('friends', function(res){
 		client.get(urls+"/users/friends/get?token="+res.token, function(data, response){
			io.emit('friends', data);
		});
	});

	// message send and recieved
  	socket.on('message', function(message){
  		var newMsg = new Chat({message: '' + message.message, sender: '' + message.sender, receiver: '' + message.receiver});
   		 // io.emit('message', message);
  		  newMsg.save(function(err){
            if(err) throw err;
                io.emit('message', message);
        });
  	});

  	// user complete chat get
	socket.on('getChat', function(res){
		Chat.find({}, function(err, chats){
			if(!err)
			{
				io.emit('getChat', chats);
			}
		});
	});

	// user disconnect
	socket.on('disconnect', function() {
    	console.log('disconnected', user_id);
	});
  
});
// socket io end


mongoose.connect('mongodb://188.166.182.26:27017/rajneetilive', function(err, db){
	if(err){
		
	} else{
    	console.log('Connected to mongodb!');
	}
});

http.listen(3000);