.controller('homeContainerCtrl', ['$scope','$rootScope','$filter', 'Page','loginModal','signupModal', function($scope,$rootScope,$filter,Page,loginModal,signupModal){

	$rootScope.isGuest= true;
	
	Page.setTitle($filter('translate')('HOME_TITLE'));

	$scope.loginModalStatus= function(){return loginModal.get()};
	$scope.showLoginModal= function(){ loginModal.show(); signupModal.hide()};

	$scope.signupModalStatus= function(){return signupModal.get()};
	$scope.showSignupModal= function(){ signupModal.show(); loginModal.hide()};

}])