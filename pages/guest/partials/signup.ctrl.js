.controller('signupCtrl', ['$scope','signupModal','$mdToast', 'Assemblies', 'Villages', 'Parties', 'Toast', '$filter', 'UserSignup', '$localStorage', '$state','States', function($scope,signupModal,$mdToast, Assemblies, Villages, Parties, Toast, $filter, UserSignup, $localStorage, $state,States){
	// get states
  States.get({}, function(response){
    response.$promise.then(function(data){
      $scope.states = data.states;
    });
  });

  // get assemblies
  $scope.statesChange = function(){
    Assemblies.get({state:$scope.user.state},function(response){
      response.$promise.then(function(assembly){
        $scope.assemblies = assembly.data;
      });
    });
  }
  // get parties
  Parties.get({}, function(response){
    response.$promise.then(function(party){
      $scope.parties = party.data;
    });
  });
  

  $scope.modalStatus= function(){return signupModal.get()};

  $scope.closeSignupModal= function(){ signupModal.hide()};

  // assembly accordig  to get the village
  $scope.assemblyChange = function(assembly){
    $scope.assembly = assembly;
    Villages.get({type:$scope.assembly},function(response){
      response.$promise.then(function(villages){
          $scope.wards = villages.data;
         
      });
    });
  };

function successAuth(res) {
      $localStorage.token = res.token;
      $localStorage.token_id = res.token_id;
      $localStorage.authId = res.id;
      Toast.queue($filter('translate')('LOGIN_MSG'));
      window.location="/verify";
}
  $scope.user={};
  $scope.submit= function(){
    $scope.signupLoading = true;
    var data = $scope.user;
      UserSignup.create(data, function (response) {
        $scope.signupLoading = false;
        console.log(response);
        $scope.loginLoading=false;
        
          // $mdToast.show(
          //   $mdToast.simple()
          //   .content('Could not login!')
          //   .position('top left')
          //   .hideDelay(3000)
          // );
          if(response.error == false)
          {
            successAuth(response);
          }
        
          if(response.errorMsg == 'already')
          $scope.signupLoading=false;
          $scope.messageError = "Your Mobile number already registered";

      }, function(error){
        $scope.signupLoading=false;
         $scope.messageError = "Your Mobile number already registered";
      });

  };
   
 }])