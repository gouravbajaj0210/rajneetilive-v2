.controller('langCtrl', ['$scope','$state','locale', function($scope,$state,locale){

	$scope.languages=[{'title':'English','locale':'EN'},{'title':'हिंदी','locale':'HIN'},{'title':'ਪੰਜਾਬੀ','locale':'PUN'}];

	$scope.setLocale= function(){
		locale.setLocale($scope.currentLanguage);
		$state.reload();
	}

	$scope.currentLanguage= locale.getLocale();
}])