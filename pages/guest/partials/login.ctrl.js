.controller('loginCtrl', ['$scope','loginModal','Auth','$localStorage','$mdToast','Toast','$state','$filter','urls', function($scope,loginModal,Auth,$localStorage,$mdToast,Toast,$state,$filter, urls){
	
	$scope.modalStatus= function(){return loginModal.get()};

	$scope.closeLoginModal= function(){ loginModal.hide()};

	function successAuth(res) {
	    $localStorage.token = res.token;
	    $localStorage.authId = res.id;
	    $localStorage.verify = res.verify;
	    
	   socket.emit('userStatus', {url:urls.API+"/user/status/online/"+res.id+"?token="+res.token, id:res.id});
	   if(res.verify == '1')
	   {
	   		Toast.queue($filter('translate')('LOGIN_MSG'));
	   		window.location = "/";
	   }
	   else
	   {
	   	window.location="/verify";
	   }
	   $state.reload();
	}

	$scope.login = function () {
		$scope.loginLoading=true;

	    var formData = {
	        mobile: $scope.user.mobile,
	        password: $scope.user.password
	    };

	    

	    Auth.login(formData, successAuth, function (resp) {
	    	$scope.loginLoading=false;
	        $mdToast.show(
	          $mdToast.simple()
	          .content(resp.message)
	          .position('top left')
	          .hideDelay(3000)
	        );
	    })
	};

}])