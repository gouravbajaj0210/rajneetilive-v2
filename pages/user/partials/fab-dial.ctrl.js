.controller('fabDialCtrl',['$scope','$mdDialog', function($scope,$mdDialog){

  $scope.fabOptions=[
  {'title' : 'Add Audio','name': 'audio','icon' : 'audiotrack'},
  {'title' : 'Add Video','name': 'video','icon' : 'videocam'},
  {'title' : 'Add Images','name': 'images', 'icon' : 'image'},
  {'title' : 'Add Status','name': 'status', 'icon' : 'send'}
  ];



  function addStatusCtrl($scope,option){
    $scope.option=option;

    $scope.closeModal = function() {
      $mdDialog.cancel();
    };
  }

  $scope.submit= function(ev,option,user){


    $mdDialog.show({
      controller: addStatusCtrl,
      templateUrl: 'pages/user/partials/status-modal.view.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      escapeToClose : false,
      locals: {option:option}
    });
  };

}])