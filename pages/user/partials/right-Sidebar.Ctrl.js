.controller('rightSide',['$scope','$rootScope','$q','$http','urls','$localStorage','$state','User', function($scope,$rootScope,$q,$http,urls,$localStorage,$state,User){
  
  $scope.IMAGE_PATH= urls.IMAGE_PATH;
  $scope.authId = $localStorage.authId;
  User.get({},function(user){
    $scope.user= user.data;  
  });
  
  $scope.messages =[];
  $http.get(urls.API+"/user/party").then(function(response){
  	$scope.party = response.data.likes;
  });

  $scope.friends="";
  var imagePath = 'http://api.rajneeti.dev/images/profile/default_avatar.png';
  $scope.IMAGE_PATH = urls.IMAGE_PATH;
    $http.get(urls.API+'/users/friends/get').then(function(resp){
      $scope.friends = resp.data.friends;
      $scope.totalOnline = resp.data.totalOnline;
    });
    socket.emit('friends', {token:$localStorage.token});
    socket.on('friends', function(data){
      $scope.friends = data.friends;
      $scope.totalOnline = data.totalOnline;
    });

    
  $scope.chatPopups = $localStorage.popup;
 
  $scope.close = function(index)
  {
  	 $localStorage.popup.splice(index, 1); 
  };

  if($localStorage.popup === undefined)
  {
    $scope.items=[];
  }
  else
  {
    $scope.items = $localStorage.popup;
  }
  $scope.openChat = function(index)
  {
    socket.emit('getChat', {sender:$scope.authId, receiver:$scope.friends[index]['id']});
    $scope.items.push($scope.friends[index]);
    $localStorage.popup = $scope.items;
    $state.reload();
  };

  if($localStorage.popup !== undefined)
  {
    for(var b = 0;b<$localStorage.popup.length;b++)
    {
      socket.emit('getChat', {sender:$scope.authId, receiver:$localStorage.popup[b]['id']});
    }
  }

  socket.on('getChat', function(data){
    for(var i=0;i<data.length;i++)
    {
      $scope.messages.push(data[i]);
    }
  });

  

  
  $scope.sendMessage = function(id, message)
  {
    socket.emit('message', {message:message, sender:$scope.authId, receiver:id});
  }
  // message send and output recieve 
    socket.on('message', function(data){
      $scope.messages.push(data);
      console.log(data);
    });

}])