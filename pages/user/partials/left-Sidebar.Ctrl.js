.controller('leftSideCtrl',['$scope','$rootScope','Page','Posts','addPosts','$q','$mdDialog','$http','urls','$state', function($scope,$rootScope,Page,Posts,addPosts,$q,$mdDialog,$http, urls,$state){
  
 $scope.joinGroup = function()
 {
 	$state.go('user.messages.groups');
 };

  $scope.IMAGE_PATH = urls.IMAGE_PATH;

  	$http.get(urls.API+"/group/get/users").then(function(success){
  		$scope.groups = success.data.result;
  	});

  	$scope.openGroup = function(slug)
  	{
  		$state.go('user.messages.closeGroup', {"group":slug});
  	}
}])