.controller('tabsCtrl', function($scope, $state, $log) {

    $scope.$watch('selectedTab', function(current, old) {
        switch (current) {
            case 0:
                $state.transitionTo('user.timeline.public');
                break;
            case 1:
                $state.transitionTo('user.timeline.party');
                break;
            case 2:
                $state.transitionTo('user.timeline.assembly');
                break;
        }
    });
})