.controller('videoCtrl', ['$scope','Posts','Upload','urls', '$localStorage', '$state', function($scope,Posts,Upload,urls, $localStorage, $state){
$scope.shareButton=false;
  $scope.uploadedVideo="";
  $scope.IMAGE_PATH = urls.IMAGE_PATH;
  // for multiple files:
    $scope.uploadFiles = function (files) {
      if (files && files.length) {
        for (var k = 0; k < files.length; k++) {

          if(typeof $scope.images == 'undefined')
            $scope.images=[];

          $scope.images.push(files[k]);

          files[k].id= $scope.images.length-1;
     
        }

        for (var i = 0; i < files.length; i++) {

          Upload.upload({
            url: urls.API + '/posts/video',
            data: {video: files[i]}
        }).then(function (resp) {
          $scope.uploadedVideo = resp.data.video;
           if($scope.uploadedVideo != null)
          {
            $scope.shareButton=true;
          }
        }, function (resp) {
        }, function (evt) {
          var progressPercentage= parseInt(100.0 * evt.loaded / evt.total);

          $scope.images[evt.config.data.video.id]['progress']= progressPercentage;
        });

     
        }
      }
    };
  
$scope.shareVideo = function(){
  if( (typeof $scope.uploadedVideo !== 'undefined' ? $scope.uploadedVideo.length : 0 )){
      Posts.save({video: $scope.uploadedVideo, type: 'video'}, function(response){
        $scope.$parent.closeModal();
        $state.reload();
      }, function(response){
      });

      $scope.statusMsg="";

      }
  };

}])