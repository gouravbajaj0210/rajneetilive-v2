.controller('imagesCtrl', ['$scope','Posts','Upload','urls', '$localStorage', '$state', function($scope,Posts,Upload,urls, $localStorage, $state){
  $scope.uploadedImages=[];
  $scope.shareButton=false;

  // for multiple files:
    $scope.uploadFiles = function (files) {
      if (files && files.length) {
        for (var k = 0; k < files.length; k++) {

          if(typeof $scope.images == 'undefined')
            $scope.images=[];

          $scope.images.push(files[k]);

          files[k].id= $scope.images.length-1;
     
        }

        for (var i = 0; i < files.length; i++) {

          Upload.upload({
            url: urls.API + '/posts/image',
            data: {image: files[i]}
        }).then(function (resp) {
          $scope.uploadedImages.push(resp.data.image);
          if($scope.uploadedImages != null)
          {
            $scope.shareButton=true;
          }

        }, function (resp) {
        }, function (evt) {
          var progressPercentage= parseInt(100.0 * evt.loaded / evt.total);

          $scope.images[evt.config.data.image.id]['progress']= progressPercentage;
        });

     
        }
      }
    };
  
$scope.shareImage = function(){

  if( (typeof $scope.uploadedImages !== 'undefined' ? $scope.uploadedImages.length : 0 )){
      Posts.save({images: angular.fromJson(angular.toJson($scope.uploadedImages)), type: 'images'}, function(response){
        $scope.$parent.closeModal();
        $state.reload();
      }, function(response){
      });

      $scope.statusMsg="";

      }
  };

}])