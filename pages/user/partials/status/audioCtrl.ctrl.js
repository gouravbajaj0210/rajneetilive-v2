.controller('voiceCtrl', ['$scope','Posts','Upload','urls', '$state', function($scope, Posts, Upload, urls, $state){
  $scope.uploadedAudio="";
  $scope.shareButton=false;
  $scope.IMAGE_PATH = urls.IMAGE_PATH;
  // for multiple files:
  $scope.uploadFiles = function (files) {

    if (files && files.length) {
      for (var k = 0; k < files.length; k++) {

        if(typeof $scope.images == 'undefined')
          $scope.images=[];

        $scope.images.push(files[k]);

        files[k].id= $scope.images.length-1;

      }

      for (var i = 0; i < files.length; i++) {

        Upload.upload({
          url: urls.API + '/posts/audio',
          data: {audio: files[i]}

        }).then(function (resp) {
          $scope.uploadedAudio = resp.data.audio;
           if($scope.uploadedAudio != null)
          {
            $scope.shareButton=true;
          }
        }, function (resp) {
        }, function (evt) {
          var progressPercentage= parseInt(100.0 * evt.loaded / evt.total);

          $scope.images[evt.config.data.audio.id]['progress']= progressPercentage;
        });

      }
    }
  };

  $scope.shareAudio = function(id){
  
  if( (typeof $scope.uploadedAudio !== 'undefined' ? $scope.uploadedAudio.length : 0 )){
      Posts.save({audio: $scope.uploadedAudio, type: 'audio'}, function(response){
        $scope.$parent.closeModal();
        $state.reload();
      }, function(response){
      });

      $scope.statusMsg="";

      }
  };

}])
