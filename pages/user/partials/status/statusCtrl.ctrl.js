.controller('statusCtrl', ['$scope','Posts', '$state', function($scope,Posts, $state){

  $scope.addStatus= function(){

  	if( (typeof $scope.statusMsg !== 'undefined' ? $scope.statusMsg.length : 0 ) > 2){
      Posts.save({status: $scope.statusMsg, type: 'text'}, function(response){
        $scope.$parent.closeModal();
        $state.reload();
      }, function(response){
      });

      $scope.statusMsg="";

      }
  };

}])