.controller('navbarCtrl',['$scope','$mdSidenav','Auth', '$mdDialog', '$state', 'Notification', '$timeout', '$interval', 'NotificationGet', 'urls', 'Upload', 'Group', '$mdToast', 'BuddyRequest','Parties','User','$localStorage', function($scope,$mdSidenav,Auth, $mdDialog, $state, Notification, $timeout, $interval, NotificationGet, urls, Upload, Group, $mdToast, BuddyRequest,Parties,User,$localStorage){
  $scope.toggleSidenav = function(menuId) {
    $mdSidenav(menuId).toggle();
  };


  $scope.IMAGE_PATH = urls.IMAGE_PATH;
  
  $scope.notiCount=0;

  $scope.goHome = function()
  {
    // window.location="#/home";
  	 $state.go('user.timeline.public');
  };

  $scope.seeAllBuddy = function()
  {
    $state.go('user.buddy.allBuddy');
  };

  $scope.messages = function()
  {
    // window.location="#/messages";
    $state.go('user.messages.msg');
  };

   $scope.logout= function(){
    socket.emit('userStatus', {url:urls.API+"/user/status/offline/"+$localStorage.authId+"?token="+$localStorage.token});
    $scope.logoutLoading=true;
    $localStorage.verify = '';
    Auth.logout();
  }

  $scope.requestFound = false;

  this.loadNotifications = function (){
      Notification.notification().then(function(data){
        $scope.notiCount = data.data.count;
        $scope.request = data.data.request;
        if($scope.request != '0')
          {
            $scope.requestFound = true;
          }
      });
   };

   //Put in interval, first trigger after 10 seconds 
   $interval(function(){
      this.loadNotifications();
   }.bind(this), 1000);    

   //invoke initialy
   this.loadNotifications();

$scope.notifiGet = function($mdOpenMenu, ev){
    $mdOpenMenu(ev);
    NotificationGet.getNotification().then(function(data){
      $scope.notifications = data.data.notification;
    });
    NotificationGet.View().then(function(data){
    });
}; 
$scope.loading = true;  
$scope.requestsGet = '';
$scope.buddyRequest = function($mdOpenMenu, ev){
    $mdOpenMenu(ev);
    BuddyRequest.getRequest().then(function(response){
      $scope.requestsGet =response.data.request;
      $scope.loading = false;  
    }, function(response){
      $scope.loading = false;  
    });
    BuddyRequest.view();
};

$scope.acceptRequest = function(buddyID, ev)
{
  $scope.requestsGet[ev].view = '1';
  BuddyRequest.accept(buddyID).then(function(response){
  });
}

$scope.closeCreateGroup = function() {
      $mdDialog.cancel();
};

$scope.createGroup = function(menu){
  $mdSidenav(menu)
   .close()
   .then(function(){
     // $log.debug('closed');
   });
   $mdDialog.show({
      templateUrl: 'pages/user/partials/createGroup.view.html',
      parent: angular.element(document.body),
      // targetEvent: ev,

      clickOutsideToClose:false,      
      // locals: {currentPost:currentPost,IMAGE_PATH:$scope.IMAGE_PATH}
    });
};
 $scope.uploadedImages='';
 $scope.shareButton = false;

$scope.updateGroupImage = function(files){
   if (files && files.length) {
        for (var k = 0; k < files.length; k++) {

          if(typeof $scope.images == 'undefined')
            $scope.images=[];

          $scope.images.push(files[k]);

          files[k].id= $scope.images.length-1;
     
        }
        for (var i = 0; i < files.length; i++) {

          Upload.upload({
            url: urls.API + '/posts/group-image',
            data: {image: files[i]}
        }).then(function (resp) {
          $scope.uploadedImages=resp.data.image;

          if($scope.uploadedImages != null)
          {
            $scope.shareButton=true;
          }
          
        }, function (resp) {
        }, function (evt) {
          var progressPercentage= parseInt(100.0 * evt.loaded / evt.total);

          $scope.images[evt.config.data.image.id]['progress']= progressPercentage;
        });

     
        }
      }
};


$scope.submitGroup = function(){
  
  $scope.data = {name:$scope.group.name, image:$scope.uploadedImages};
    Group.create($scope.data, function(resp){
      $mdDialog.hide();
      $state.go('user.messages.closeGroup', {group:resp.slug});
    }, function(error){
      if(error.message == 'notvalid')
      {
          $mdToast.show(
            $mdToast.simple()
            .content("This name already Register")
            .position('top left')
            .hideDelay(5000)
          );
      }
    });
  };

  $scope.allParty = function(menu)
  {
    $mdSidenav(menu)
      .close()
   .then(function(){
     // $log.debug('closed');
   });
    $mdDialog.show({
      templateUrl: 'pages/user/partials/allParties.view.html',
      parent: angular.element(document.body),
      // targetEvent: ev,

      clickOutsideToClose:false,      
      // locals: {currentPost:currentPost,IMAGE_PATH:$scope.IMAGE_PATH}
    });
  };

   Parties.get({}, function(response){
    response.$promise.then(function(party){
      $scope.parties = party.data;
    });
  });

  User.get({},function(user){
    $scope.user= user.data;  
  });

  $scope.openPage = function(slug)
  {
    $mdDialog.cancel();
    $state.go('parties.page', {party:slug});
  }  

}])