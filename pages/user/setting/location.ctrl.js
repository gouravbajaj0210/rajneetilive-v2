.controller('locationCtrl',['$scope','Page','$sce', 'User', 'UpdateInfo', '$state','States','Assemblies','Villages','$mdDialog', function($scope,Page,$sce, User, UpdateInfo, $state,States,Assemblies,Villages,$mdDialog){
  Page.setTitle("Location");

function getData(){
  User.get({}, function(resp){
  	resp.$promise.then(function(data){
  		$scope.user = data.data;
  		Assemblies.get({state:data.data.state_id}, function(respo){
  			respo.$promise.then(function(assem){
  				$scope.assemblies = assem.data;
  				Villages.get({type:data.data.assembly_id}, function(vill){
  					vill.$promise.then(function(response){
  						$scope.villages = response.data;
  					});
  				});
  			});
  		});
  	});
  });
};
getData();
  States.get({}, function(response){
    response.$promise.then(function(data){
      $scope.states = data.states;
    });
  });

	$scope.stateChange = function(){
		console.log($scope.user.state_id);
		UpdateInfo.change({value:$scope.user.state_id, type:'state'}, function(response){
			getData();
	       var confirm = $mdDialog.confirm()
	         .title('Successfully Change State')
	         .targetEvent()
	         .ok('Okay')
	         $mdDialog.show(confirm).then(function() {
	          
	        }, function() {
	          
	        });
	    });
	};
  
  $scope.assemblyChange = function(){
		UpdateInfo.change({value:$scope.user.assembly_id, type:'assembly'}, function(response){
			getData();
	       var confirm = $mdDialog.confirm()
	         .title('Successfully Change Assembly')
	         .targetEvent()
	         .ok('Okay')
	         $mdDialog.show(confirm).then(function() {
	        	 
	        }, function() {
	          
	        });
	    });
	};

	$scope.villageChange = function(){
		UpdateInfo.change({value:$scope.user.village_id, type:'village'}, function(response){
			getData();
	       var confirm = $mdDialog.confirm()
	         .title('Successfully Change Assembly')
	         .targetEvent()
	         .ok('Okay')
	         $mdDialog.show(confirm).then(function() {
	        	 
	        }, function() {
	          
	        });
	    });
	};
  

}])