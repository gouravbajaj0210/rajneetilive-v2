.controller('generalCtrl',['$scope','Page','$sce', 'User', 'UpdateInfo', '$state','$mdDialog', function($scope,Page,$sce, User, UpdateInfo, $state,$mdDialog){
  Page.setTitle("General Setting");
  $scope.name="my data";
  User.get({},function(response){
    $scope.user= response.data;
     // $scope.user.emailalert =$scope.userdetail.emailAlert;
    
  });

  $scope.userName = function(){
    UpdateInfo.change({value: $scope.user.name, type: 'name'}, function(response){
        var confirm = $mdDialog.confirm()
         .title('Successfully Change Name')
         .targetEvent()
         .ok('Okay')
         $mdDialog.show(confirm).then(function() {
          
        }, function() {
          
        });
    }, function(response){
    });
  };

  $scope.checkUserName = function(){
    UpdateInfo.userName({value:$scope.user.slug, type:'name'}, function(response){
      if(response.message == 'already')
      {
        $scope.error=true;
      }
      else
      {
       $scope.error=false; 
      }
    });
  };
  
  $scope.userslug = function(){
    UpdateInfo.change({value:$scope.user.slug, type:'slug'}, function(response){
       var confirm = $mdDialog.confirm()
         .title('Successfully Change Username')
         .targetEvent()
         .ok('Okay')
         $mdDialog.show(confirm).then(function() {
          
        }, function() {
          
        });
    });
  };

  $scope.checkemail = function(){
    UpdateInfo.userName({value:$scope.user.email, type:'email'}, function(response){
      if(response.message == 'already')
      {
        $scope.emailError = true;
      }
      else
      {
       $scope.emailError = false; 
      }
    });
  };
   
  $scope.email = function(){
    UpdateInfo.change({value:$scope.user.email, type:'email'}, function(response){
       var confirm = $mdDialog.confirm()
         .title('Successfully Change Email')
         .targetEvent()
         .ok('Okay')
         $mdDialog.show(confirm).then(function() {
          
        }, function() {
          
        });
    });
  } ;

  $scope.mobileCheck = function(){
    UpdateInfo.userName({value:$scope.user.mobile, type:'mobile'}, function(response){
      if(response.message == 'already')
      {
        $scope.mobileError = true;
      }
      else
      {
        $scope.mobileError = false;
      }
    });
  };

  $scope.mobile = function(){
    UpdateInfo.change({value:$scope.user.mobile, type:'mobile'}, function(response){
      console.log(response.data);
      if(response.data == 'notsame')
      {
        $state.go('info.otp');
      }
    });
  };

  $scope.emailAlert = function(){
    if($scope.user.emailalert == 'true')
    {
      var value = 'Active';
    }else{
      var value = 'Deactive';
    }
    UpdateInfo.change({value:$scope.user.emailalert, type:'alert'}, function(response){
       var confirm = $mdDialog.confirm()
         .title('Successfully '+value+' Email Notification')
         .targetEvent()
         .ok('Okay')
         $mdDialog.show(confirm).then(function() {
          
        }, function() {
          
        });
    });
  }

  $scope.oldPassCheck = function(){
    UpdateInfo.userName({value:$scope.user.oldpassword, type:'password'}, function(response){
      if(response.message =='not')
      {
        $scope.passwordError = true;
      }
      else
      {
       $scope.passwordError = false; 
      }
    });
  };

  $scope.changePassword = function(){
      UpdateInfo.change({oldpassword:$scope.user.oldpassword, newpassword:$scope.user.newpassword, type:'password'}, function(response){
        $scope.user.oldpassword="";
        $scope.user.newpassword="";
        $state.reload();
          var confirm = $mdDialog.confirm()
         .title('Successfully to change Password')
         .targetEvent()
         .ok('Okay')
         $mdDialog.show(confirm).then(function() {
          
        }, function() {
          
        });
      });
  };
  
}])