.controller('closeGroupCtrl',['$scope','Page','$stateParams','$localStorage','$state','urls','singleGroup', '$mdDialog', 'User','$http','Upload','Group', function($scope,Page, $stateParams, $localStorage, $state, urls, singleGroup, $mdDialog, User,$http,Upload,Group){
  Page.setTitle("Message");
	$scope.authId = $localStorage.authId;
	$scope.slug = $stateParams.group;
	$scope.IMAGE_PATH = urls.IMAGE_PATH;

	singleGroup.check({user:$scope.authId}, function(response){
		response.$promise.then(function(user){

			$scope.loginUserType = user.users;
			if(user.users == 'notMember')
			{
				$state.go('user.messages.groups');
			}
		});
	});

	singleGroup.get({slug:$scope.slug},function(response){
    	response.$promise.then(function(group){
    		
          $scope.groupDetail = group.group;
           $scope.members = group.members;
      	});
  	});

  	$scope.addMember = function(){
	  	$mdDialog.show({
	    	templateUrl: 'pages/user/message/addMember.view.html',
	    	parent: angular.element(document.body),
	    	clickOutsideToClose:false,      
	    });
  	};

  	$scope.closeCreateGroup = function() {
      $mdDialog.cancel();
      $state.reload();
	};
	$scope.Loading=false;

	$scope.add = function(groupID, userID){
		$scope.Loading= true;
		
		singleGroup.Add({type:'add',group:groupID, user:userID}, function(response){
			$scope.Loading=false;
			$scope.getUsers();

		});
	};

	$scope.remove = function(groupID, userID){
		$scope.Loading=true;
		singleGroup.Add({type:'remove', group:groupID, user:userID}, function(response){
			$scope.Loading= false;
			$scope.getUsers();
		});
	};

	$scope.getUsers = function(){
	  	singleGroup.allUser({slug:$scope.slug}, function(response){
	  		response.$promise.then(function(users){
	  			return $scope.users = users.users;
	  		});
	  	});
  	};
  	$scope.getUsers();

  	 $scope.showAlert = function(ev, id) {
	   var confirm = $mdDialog.confirm()
	   .title('are you sure you want to delete?')
	   .targetEvent(ev)
	   .ok('Okay')
	   .cancel('Cancel');
	   $mdDialog.show(confirm).then(function() {
	    
	    singleGroup.delete({id:id}, function(success){
	       $state.go('user.messages.groups');
	    });
	  }, function() {
	    $scope.status = 'You decided to keep your debt.';
		});
 	};

 	$scope.leftGroup = function(id, groupID)
 	{
 		singleGroup.left({id:id, groupID:groupID}, function(success){
 			$state.go('user.messages.groups');
 		});
 	};

 	$scope.groupEdit = function()
 	{
 		$mdDialog.show({
      		templateUrl: 'pages/user/partials/groupEdit.view.html',
      		parent: angular.element(document.body),
    		clickOutsideToClose:false,      
    	});
 	};

 	$scope.closeCreateGroup = function() {
      $mdDialog.cancel();
	};

	$scope.uploadedImages = '';
	$scope.updateGroupImage = function(files){
		
   if (files && files.length) {
        for (var k = 0; k < files.length; k++) {

          if(typeof $scope.images == 'undefined')
            $scope.images=[];

          $scope.images.push(files[k]);

          files[k].id= $scope.images.length-1;
     
        }
        
        for (var i = 0; i < files.length; i++) {

          Upload.upload({
            url: urls.API + '/posts/group-image',
            data: {image: files[i]}
        }).then(function (resp) {
          
          $scope.uploadedImages=resp.data.image;

          if($scope.uploadedImages != null)
          {
            $scope.shareButton=true;
          }
          
        }, function (resp) {
            
        }, function (evt) {
          var progressPercentage= parseInt(100.0 * evt.loaded / evt.total);

          $scope.images[evt.config.data.image.id]['progress']= progressPercentage;
            
        });

     
        }
      }
	};

	$scope.updateGroup = function()
	{
		if($scope.uploadedImages != '')
		{
			$scope.groupDetail.image = $scope.uploadedImages;
		}
		
		Group.update($scope.groupDetail, function(resp){
		
			$state.go('user.messages.closeGroup', {group:resp.newSlug});
			$mdDialog.cancel();
		});
	};


}])