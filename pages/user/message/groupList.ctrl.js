.controller('groupListCtrl',['$scope','Page','$sce','$mdSidenav','$filter','$http','urls','$localStorage', function($scope,Page,$sce, $mdSidenav, $filter,$http, urls,$localStorage){
	  Page.setTitle($filter('translate')('Groups'));
	  $scope.authID = $localStorage.authId;
	  
	   $scope.rightToggle = function(menuId) {
	    $mdSidenav(menuId).toggle();
	  };
	  $scope.IMAGE_PATH = urls.IMAGE_PATH;
	  $scope.groups = '';
	  $http.get(urls.API+"/group/get/all").then(function(success){
	  	$scope.groups = success.data.result;
	  });

	  $scope.join = function(ind, groupid)
	  {
	  	$scope.groups[ind].joined = 1;
	  	
	  };
	  
}])