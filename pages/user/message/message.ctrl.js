.controller('MessageCtrl',['$scope','Page','$sce', 'urls', 'chatSocket', 'User','$localStorage', '$timeout','getChat', function($scope,Page,$sce, urls, chatSocket, User, $localStorage, $timeout, getChat){
  Page.setTitle("Message");
  $scope.authId = $localStorage.authId;
  $scope.typing=false;
  $scope.IMAGE_PATH= urls.IMAGE_PATH;
   $scope.isOpen = false;
      $scope.demo = {
        isOpen: false,
        count: 0,
        selectedDirection: 'right'
      };
      var messages = {};
    User.onlineUsers(function(response){
        response.$promise.then(function(users){
          $scope.users = users.users;
          
      });
    });
   
    $scope.users = [];
    $scope.messages = [];
    chatSocket.connect();

    $scope.messageSendSelect = function(user){
    	$scope.name = user.name;
      $scope.avatar = user.avatar;
      $scope.id = user.id;
      chatSocket.emit('add-user', {userid:user.id});
      getChat.get({sender:$scope.authId, receive:$scope.id},function(response){
       
        $scope.messages =[];
        response.$promise.then(function(chat){
          
          var data = chat.chat;

          for(i=0;i<=data.length;i++)
          { 
            $scope.messages.push(data[i]);
          }
          
          
        });
      });
      
    };
    

    $scope.typingStart= function(){
      if($scope.message != '')
      {
        chatSocket.emit('status', {status:'1', received:$scope.id});
      }
      else
      {
        chatSocket.emit('status', {status:'0', received:$scope.id});
      }
    };

    chatSocket.on('status', function(data){
      $scope.status = data;
      if(data.status == 1){
        $scope.typing=true;  
      }
      else{
        $scope.typing=false;
      }
    });

    $scope.sendMessage = function(){
      if($scope.message != null && $scope.message != '')
      {
        // socket.emit('chat message', $('#m').val());
        socket.emit('message', {message:$scope.message, sender:$scope.authId, receiver:$scope.id});
        $scope.message = '';
      }
    };

    socket.on('users', function(data){
        $scope.users = data.users;
    });

    socket.on('message', function(data){
      $scope.messages.push(data);
      console.log(data);
    });

    console.log($scope.messages , 'messages');

}])