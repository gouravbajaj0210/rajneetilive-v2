  .controller('coverCtrl', ['$scope','Posts','Upload','urls', '$localStorage', '$state', 'UpdateInfo', '$mdDialog', function($scope,Posts,Upload,urls, $localStorage, $state, UpdateInfo, $mdDialog){
  $scope.uploadedImages='';
  $scope.shareButton=false;
  $scope.uploadFiles = function (files) {
      if (files && files.length) {
        for (var k = 0; k < files.length; k++) {

          if(typeof $scope.images == 'undefined')
            $scope.images=[];

          $scope.images.push(files[k]);

          files[k].id= $scope.images.length-1;
     
        }

        for (var i = 0; i < files.length; i++) {

          Upload.upload({
            url: urls.API + '/posts/cover',
            data: {image: files[i]}
        }).then(function (resp) {
          $scope.uploadedImages=resp.data.image;
          if($scope.uploadedImages != null)
          {
            $scope.shareButton=true;
          }
          
        }, function (resp) {
        }, function (evt) {
          var progressPercentage= parseInt(100.0 * evt.loaded / evt.total);

          $scope.images[evt.config.data.image.id]['progress']= progressPercentage;
        });

     
        }
      }
    };
    $scope.closeModal = function() {
      $mdDialog.cancel();
    };

    $scope.updateCover= function(){
      UpdateInfo.change({value: $scope.uploadedImages, type: 'cover'}, function(response){
        $state.reload();
         $mdDialog.cancel();
        }, function(response){
        });
    };
  }])