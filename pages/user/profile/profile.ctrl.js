.controller('profileCtrl',['$scope','Page', '$stateParams', 'Posts', '$q', 'addPosts', '$localStorage', '$mdDialog', '$mdMedia', '$state', function($scope,Page, $stateParams, Posts, $q, addPosts, $localStorage, $mdDialog, $mdMedia, $state){
  Page.setTitle($stateParams.slug);
  $scope.name=$stateParams.slug;

  function commentsCtrl($scope,$mdDialog,Posts,currentPost,IMAGE_PATH){

    $scope.post=currentPost;
    $scope.IMAGE_PATH=IMAGE_PATH;


    $scope.updated=0;

    $scope.isLoading= true;

    $scope.comments= new Array();
      
  
    Posts.comments({id:$scope.post.id},function(response){
      $scope.comments= response.data;

      if(response.data.length == 0)
        $scope.noComments=true;

      $scope.post.comment_count=response.data.length;

      $scope.isLoading= false;



    });



    $scope.closeComments = function() {

      $mdDialog.cancel();
    };

    $scope.addComment= function(){

      if( (typeof $scope.newComment !== 'undefined' ? $scope.newComment.length : 0 ) > 2){
        Posts.addComment({id:$scope.post.id,comment: $scope.newComment}, function(response){
          $scope.comments.unshift(response.data);

          $scope.updated++;
          $scope.noComments=false;

          $scope.post.comment_count++;

        });

        $scope.newComment="";
      }
    }

  }
  $scope.showCommentsProfile = function(currentPost) {
    $mdDialog.show({
      controller: commentsCtrl,
      templateUrl: 'pages/user/partials/comments.view.html',
      parent: angular.element(document.body),
      // targetEvent: ev,
      clickOutsideToClose:true,      
      locals: {currentPost:currentPost,IMAGE_PATH:$scope.IMAGE_PATH}

    });
  };

  $scope.profileCover = function(userId) {
    $mdDialog.show({
      templateUrl: 'pages/user/profile/coverModel.view.html',
      parent: angular.element(document.body),
      // targetEvent: ev,
      clickOutsideToClose:true,      
      locals: {currentPost:userId,IMAGE_PATH:$scope.IMAGE_PATH}

    });
  };

    $scope.profilePhoto = function(userId) {
    $mdDialog.show({
      templateUrl: 'pages/user/profile/profilePic.view.html',
      parent: angular.element(document.body),
      // targetEvent: ev,
      clickOutsideToClose:true,      
      locals: {currentPost:userId,IMAGE_PATH:$scope.IMAGE_PATH}

    });
  };

  



  $scope.profilePostLike= function(post,id,type){
    //like
    if(type=='like'){
      //Doesn't like already, gotta like
      if(post.i_like != 1){
        post.like_count++;
        post.i_like=1;
        Posts.like({id:id,type:type});
      }

      post.like_type=1;

      return post;
    }

    //dislike

    //Liked earlier, gotta dislike
    if(post.i_like == 1){
      post.i_like=0;
      post.like_count--;
    }

    Posts.like({id:id,type:type});

    post.like_type=0;

    return post;
  };


  $scope.showAlert = function(ev, id) {
   var confirm = $mdDialog.confirm()
   .title('are you sure you want to delete?')
   .targetEvent(ev)
   .ok('Okay')
   .cancel('Cancel');
   $mdDialog.show(confirm).then(function() {
    Posts.delete({id:id}, function(response){
       $state.reload();
    });
  }, function() {
    $scope.status = 'You decided to keep your debt.';
  });
 };


  $scope.authId = $localStorage.authId;
  
  $scope.postsLoading=true;

  $scope.postType="public";

  Posts.profile({slug:$scope.name, 'offset': 0},function(response){
    $scope.posts= response.data;
    $scope.usersDetail = response.user;
    $scope.postsLoading=false;
  });

  $scope.likePost= function(index,id,type){
    
    $scope.posts[index]= $scope.profilePostLike($scope.posts[index], id, type);
  };

   //Load more posts when required
  $scope.loadMorePosts= function(){

    var deferred = $q.defer();


    addPosts.add($scope.postType,typeof $scope.posts == 'undefined' ? [] : $scope.posts).then(function(response){
      $scope.posts= response;
      deferred.resolve(true);
    });

    return deferred.promise;
    
  }

$scope.sharePost = function(postID, userID)
   {
    Posts.share({postId:postID, userId:userID}, function(response){
      $state.reload();
    });
   };

}])