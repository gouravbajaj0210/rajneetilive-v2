.controller('nextmlaCtrl',['$scope','Page','$sce', 'Parties', 'Assemblies', 'Villages','nextCmAssembly','$http','urls','$timeout','$mdToast','$mdDialog','$localStorage', function($scope,Page,$sce, Parties, Assemblies, Villages,nextCmAssembly,$http,urls,$timeout,$mdToast,$mdDialog,$localStorage){
  Page.setTitle("NextMLA");
  $scope.name="my data";
  
   nextCmAssembly.get({},function(response){
    response.$promise.then(function(party){
      $scope.parties = party.party;
    });
    response.$promise.then(function(assembly){
      $scope.assemblies = assembly.assembly;
    });
  });

  $scope.assemblyChange = function(assembly){
    $scope.assembly = assembly;
    nextCmAssembly.village({type:$scope.assembly},function(response){
      response.$promise.then(function(villages){
          $scope.wards = villages.data;

      });
    });
  };
 
  $scope.MLAGet = function()
  {

    $http.get(urls.NEXTCM_API+"/"+$scope.opinion.assembly+"/"+$scope.opinion.party+"/mla").then(function(response){
      $scope.mlas= response.data.data;

    }, function(error){

    })
  };

  $scope.tokenID = "";
  // timer set   
        $scope.counter = 59;
        $scope.onTimeout = function(){
          if($scope.counter != 0)
          {
            $scope.counter--;
            mytimeout = $timeout($scope.onTimeout,1000);
          }
        };
        var mytimeout = $timeout($scope.onTimeout,1000);

  $scope.submit = function()
  { 
     $http.post(urls.NEXTCM_API + '/nextmla', $scope.opinion).then(function(response) {

        $localStorage.nextmlaID = response.data.data.id;
        $scope.onTimeout();
          $mdDialog.show({
              templateUrl: 'pages/user/partials/mlaOTP.view.html',
              parent: angular.element(document.body),
              clickOutsideToClose:true,      
          });
          $scope.opinion = '';
     }, function(reason){
        if(reason.data.errorMsg == 'MOBILE_USED')
                 {
                   $mdToast.show(
                      $mdToast.simple()
                      .content('This Mobile number already used')
                      .position('bottom left')
                      .hideDelay(3000)
                    ); 
                 }
                 if(reason.data.errorMsg.mobile !== undefined && reason.data.errorMsg.mobile !== null)
                 {
                   $mdToast.show(
                      $mdToast.simple()
                      .content(reason.data.errorMsg.mobile[0])
                      .position('bottom left')
                      .hideDelay(3000)
                    ); 
                 }
                 if(reason.data.errorMsg.name !== undefined && reason.data.errorMsg.name !== null)
                 {
                   $mdToast.show(
                      $mdToast.simple()
                      .content(reason.data.errorMsg.name[0])
                      .position('bottom left')
                      .hideDelay(3000)
                    ); 
                 }
                  $scope.loginLoading=false;
     });
  };

    $scope.OTPVerify = function()
    {
      var data ={token:$scope.OTP, id:$localStorage.nextmlaID};
      $http.post(urls.NEXTCM_API+'/nextmla/verify', data).then(function(response){
            if(response.data.data != 0)
            {
              $mdDialog.cancel();
                $mdToast.show(
                    $mdToast.simple()
                    .content('Your vote counted.')
                    .position('bottom left')
                    .hideDelay(3000)
                  );
                  $localStorage.nextmlaID = '';
            }
            if(response.data.data == 0)
            {
                $mdToast.show(
                    $mdToast.simple()
                    .content('Invalid OTP')
                    .position('bottom left')
                    .hideDelay(5000)
                ); 
            }
          });

    };

    $scope.closeModel = function()
    {
      $mdDialog.cancel();
    };

    $scope.resend = function()
    {
      $localStorage.tokenID;
      $http.get(urls.NEXTCM_API+'/resend/otp/'+$localStorage.nextmlaID);
            $scope.counter = 59;
            $scope.onTimeout();
    };
  

}])