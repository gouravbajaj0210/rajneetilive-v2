.controller('questionCtrl', ['$scope','Page','$stateParams','$http','urls','$mdDialog','$mdToast', function($scope, Page, $stateParams,$http,urls,$mdDialog,$mdToast){
	Page.setTitle('Unanswared');
	
	$http.get(urls.API+'/question/get/'+$stateParams.id).then(function(success){
		$scope.question = success.data.question;
	});

	$scope.submitOpinion = function(option, question){
		$scope.question.totalOpinion++;
		$scope.question.i_opinion = '1';
		
		$http.get(urls.API+'/opinion/submit/'+option+'/'+question).then(function(response){
		
			 $mdDialog.cancel();
                $mdToast.show(
                    $mdToast.simple()
                    .content('Your vote counted.')
                    .position('bottom left')
                    .hideDelay(3000)
                );
		});
	};
}])
