.controller('publicOpinionCtrl',['$scope','Page','$sce','$http','urls', function($scope,Page,$sce,$http,urls){
  Page.setTitle("Public Opinion");
  $scope.name="my data";
  $http.get(urls.API+"/public/opinion").then(function(response){
  	$scope.questions = response.data.question;
  });
}])