.controller('issueCtrl',['$scope','Page','$sce', 'Parties', 'nextCmAssembly','$http', 'urls','$mdToast', function($scope,Page,$sce, Parties,nextCmAssembly,$http,urls,$mdToast){
  Page.setTitle("Public Issue");
  $scope.name="my data";
  
  nextCmAssembly.get({},function(response){
    response.$promise.then(function(party){
      $scope.parties = party.party;
    });
    response.$promise.then(function(assembly){
      $scope.assemblies = assembly.assembly;
    });
  });

  $scope.assemblyChange = function(assembly){
    $scope.assembly = assembly;
    nextCmAssembly.village({type:$scope.assembly},function(response){
      response.$promise.then(function(villages){
          $scope.villages = villages.data;
      });
    });
  };
    
  $scope.issueSubmit = function()
  {
    $http.post(urls.NEXTCM_API + '/issue', $scope.user).then(function(response){
       $mdToast.show(
            $mdToast.simple()
                .content(response.data.data)
                .position('bottom left')
               .hideDelay(3000)
            ); 
       $scope.user = '';
    }, function(error){
    });
  };

}])