.controller('answeredCtrl', ['$scope','Page','$stateParams','$http','urls','$mdDialog','$mdToast','$timeout', function($scope, Page, $stateParams,$http,urls,$mdDialog,$mdToast,$timeout){
	Page.setTitle('Answered');

	$http.get(urls.API+"/question/result/"+$stateParams.id).then(function(success){
		$scope.question = success.data.question;
		$scope.results = success.data.result;
		for (var k = 0; k < success.data.result.length; k++) {
		      		success.data.result[k].percent=0;
		      	};
		      	$scope.results=success.data.result;

		      	$timeout(function() {
		        for (var i = 0; i < $scope.results.length; i++) {
		        	$scope.results[i].percent= $scope.results[i].per;
		        };
		}, 1500);
	});
}])