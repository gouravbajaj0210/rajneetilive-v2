.controller('buddyUpAllCtrl',['$scope','$rootScope','Page','User', 'urls','$q','BuddyUp','$localStorage','$state','$http', function($scope,$rootScope,Page,User,urls,$q,BuddyUp,$localStorage, $state,$http){
  Page.setTitle("Buddy Up");
  $scope.IMAGE_PATH = urls.IMAGE_PATH;
  $scope.authId = $localStorage.authId;
  $scope.users = '';
  $scope.userGet = function()
  {
    User.onlineUsers(function(response){
        response.$promise.then(function(users){
          $scope.users = users.users;
      });
    });
  };
  $scope.userGet();

  
  $scope.buddyUp = function(id, en){
    $scope.users[en].friend=1;
    $scope.users[en].view = 0;
    BuddyUp.send({id:id}, function(response){
    });
  };

  $scope.buddyleave = function(id, index)
  {
    $scope.users[index].friend=0;
    $http.get(urls.API+"/buddy/unbuddy/"+id).then(function(response){
      
    });
  };

}])
