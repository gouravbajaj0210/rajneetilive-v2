.controller("otpCtrl", ['$scope','Page','$timeout','User','$state','urls', '$http','$localStorage','$mdToast', function($scope, Page, $timeout,User, $state, urls, $http,$localStorage,$mdToast){
	Page.setTitle("Verification");

    User.get({},function(user){
        $scope.user= user.data;  
       if(user.data.verify == '1')
       {
            $state.go('user.timeline.public');
       }
    });


	$scope.counter = 59;
    $scope.onTimeout = function(){
    	if($scope.counter != 0)
        {
            $scope.counter--;
            mytimeout = $timeout($scope.onTimeout,1000);
        }
     };
        var mytimeout = $timeout($scope.onTimeout,1000);

    $scope.submitOTP = function()
    {
        $http.post(urls.API+"/otp/verify/"+$localStorage.authId, $scope.user).then(function(response){
            if(response.data.message == 'success')
            {
                $localStorage.verify=1;
                $state.go('user.timeline.public');
                 $mdToast.show(
                $mdToast.simple()
                    .content('Successfully Submit')
                    .position('bottom left')
                    .hideDelay(3000)
                );
            }
        }, function(error){
            $mdToast.show(
            $mdToast.simple()
                .content('Invalid OTP')
                .position('top left')
                .hideDelay(3000)
            );
        });
    };

    $scope.resend = function()
    {
        $http.get(urls.API+"/otp/resend/"+$localStorage.authId);
        $scope.counter = 59;
        $scope.onTimeout();
    }
}])