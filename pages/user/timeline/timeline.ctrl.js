.controller('timelineCtrl',['$scope','Posts','$mdDialog', '$localStorage', '$mdMedia', '$state', '$mdSidenav', function($scope,Posts,$mdDialog, $localStorage, $mdMedia, $state, $mdSidenav){
  $scope.authId = $localStorage.authId;
   
   $scope.rightToggle = function(menuId) {
    $mdSidenav(menuId).toggle();
  };
  
  
  function commentsCtrl($scope,$mdDialog,Posts,currentPost,IMAGE_PATH){

    $scope.post=currentPost;

    $scope.IMAGE_PATH=IMAGE_PATH;


    $scope.updated=0;

    $scope.isLoading= true;

    $scope.comments= new Array();
    

    Posts.comments({id:$scope.post.id},function(response){
      $scope.comments= response.data;

      if(response.data.length == 0)
        $scope.noComments=true;

      $scope.post.comment_count=response.data.length;

      $scope.isLoading= false;



    });


    // console.log($scope.myd);

    $scope.closeComments = function() {

      $mdDialog.cancel();
    };

    $scope.addComment= function(){

      if( (typeof $scope.newComment !== 'undefined' ? $scope.newComment.length : 0 ) > 2){
        Posts.addComment({id:$scope.post.id,comment: $scope.newComment}, function(response){
          $scope.comments.unshift(response.data);

          $scope.updated++;
          $scope.noComments=false;

          $scope.post.comment_count++;

        });

        $scope.newComment="";
      }
    }

  }
  $scope.showComments = function(currentPost) {
    
    $mdDialog.show({
      controller: commentsCtrl,
      templateUrl: 'pages/user/partials/comments.view.html',
      parent: angular.element(document.body),
      // targetEvent: ev,
      clickOutsideToClose:true,      
      locals: {currentPost:currentPost,IMAGE_PATH:$scope.IMAGE_PATH}
    });

  };

  $scope.groupOpen = function(slug)
  {
    $state.go('user.messages.closeGroup', {group:slug});
  }

  $scope.likeThePost= function(post,id,type){
    
    //like
    if(type=='like'){

      //Doesn't like already, gotta like
      if(post.i_like != 1){
        post.like_count++;
        post.i_like=1;
        Posts.like({id:id,type:type});
      }

      post.like_type=1;
      
      return post;
    }

    //dislike

    //Liked earlier, gotta dislike
    if(post.i_like == 1){
      post.i_like=0;
      post.like_count--;
    }

    Posts.like({id:id,type:type});

    post.like_type=0;

    return post;
  };


  $scope.showAlert = function(ev, id) {
   var confirm = $mdDialog.confirm()
   .title('are you sure you want to delete?')
   .targetEvent(ev)
   .ok('Okay')
   .cancel('Cancel');
   $mdDialog.show(confirm).then(function() {
    Posts.delete({id:id}, function(response){
       $state.reload();
    });
  }, function() {
    $scope.status = 'You decided to keep your debt.';
  });
 };


   $scope.profileview = function(slug)
   {
      $mdSidenav(menu)
      .close()
     .then(function(){
       // $log.debug('closed');
     });
      $state.go('user.profile',{ "slug": slug});
   };

   $scope.sharePost = function(postID, userID)
   {
    Posts.share({postId:postID, userId:userID}, function(response){
      $state.reload();
       var confirm = $mdDialog.confirm()
         .title('Successfully Share Repost')
         .targetEvent()
         .ok('Okay')
         $mdDialog.show(confirm).then(function() {
          
        }, function() {
          
        });
    });
   };

   $scope.zoomImage = function(images, size, name)
   {
      $mdDialog.show({
        controller: imageCtrl,
        templateUrl: 'pages/user/partials/zoomImage.view.html',
        parent: angular.element(document.body),
        clickOutsideToClose:false,   
        locals: {currentPost:images, IMAGE_PATH:$scope.IMAGE_PATH, size:size, name:name}   
      });
   };

   function imageCtrl($scope,$mdDialog,currentPost, IMAGE_PATH, size, name){
    $scope.zoomimage = currentPost;
    $scope.IMAGE_PATH = IMAGE_PATH;
    $scope.name = name;
    $scope.size = size;

       $scope.closeImage = function()
       {
          $mdDialog.cancel();
       }

         $scope._Index = 0;
 
    // if a current image is the same as requested image
    $scope.isActive = function (index) {
        return $scope._Index === index;
    };

      // show prev image
    $scope.showPrev = function () {
        $scope._Index = ($scope._Index > 0) ? --$scope._Index : $scope.zoomimage.length - 1;
    };

    
    // show next image
    $scope.showNext = function () {
        $scope._Index = ($scope._Index < $scope.zoomimage.length - 1) ? ++$scope._Index : 0;
    };

    // show a certain image
    $scope.showPhoto = function (index) {
        $scope._Index = index;
    };

   };
 
   
}])











