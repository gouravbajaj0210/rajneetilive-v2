.controller('timelineAssemblyCtrl',['$scope','$rootScope','Page','Posts','addPosts','$q', function($scope,$rootScope,Page,Posts,addPosts,$q){
  Page.setTitle("Assembly timeline");
  $rootScope.selectedTab=2;

  $scope.postsLoading=true;

  $scope.postType="assembly";
  

   // Get the initial posts
  Posts.get({type:$scope.postType, 'offset': 0},function(response){
    $scope.posts= response.data;
    $scope.postsLoading=false;  
  }, function(){
    
  });
  

  $scope.likePost= function(index,id,type){
    $scope.posts[index]= $scope.likeThePost($scope.posts[index],id,type);
  };

  //Load more posts when required
  $scope.loadMorePosts= function(){
    var deferred = $q.defer();


    addPosts.add($scope.postType,typeof $scope.posts == 'undefined' ? [] : $scope.posts).then(function(response){
      $scope.posts= response;
      deferred.resolve(true);
    });

    return deferred.promise;    
  }
}])