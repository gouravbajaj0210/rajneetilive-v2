.controller('timelinePublicCtrl',['$scope','$rootScope','Page','Posts','addPosts','$q', 'urls', '$localStorage', function($scope,$rootScope,Page,Posts,addPosts,$q, urls, $localStorage){
  Page.setTitle("Public timeline");
  $rootScope.selectedTab=0;

  $scope.IMAGE_PATH = urls.IMAGE_PATH;
  

  $scope.postsLoading=false;

  $scope.postType="public";

  $scope.closeComments = function() {

    alert('kokok');
    // $mdDialog.cancel();
  };

  $scope.likePost= function(index,id,type){

    $scope.posts[index]= $scope.likeThePost($scope.posts[index],id,type);

  };

  // Get the initial posts
  Posts.get({type:$scope.postType, 'offset': 0},function(response){
    $scope.posts= response.data;
    $scope.postsLoading=false;
  });


 
  //Load more posts when required
  $scope.loadMorePosts= function(){

    var deferred = $q.defer();



    addPosts.add($scope.postType,typeof $scope.posts == 'undefined' ? [] : $scope.posts).then(function(response){
      $scope.posts= response;
      deferred.resolve(true);
    });

    return deferred.promise;
    
  }



}])