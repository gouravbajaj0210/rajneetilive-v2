.controller('partyPageCtrl', ['$scope','Page','$http','urls','$stateParams','Auth', function($scope,Page, $http, urls,$stateParams,Auth){
Page.setTitle('party Page');	
	$http.get(urls.API+"/party/page/"+$stateParams.party).then(function(response){
		$scope.detail = response.data.result;
		Page.setTitle($scope.detail.eng_name);
		$scope.national= response.data.national_president;
		$scope.state_parbhari= response.data.state_parbhari;
		$scope.state_president= response.data.state_president;
		$scope.candidates = response.data.candidates;
	});
	
	$scope.detail = {i_like:0};

	$scope.like = function(detail, type){
		if(Auth.loggedIn())
		{
			console.log('login is');
			if(type == 'like')
			{
				detail.i_like = 1;
				detail.likes++;
			}
			else
			{
				detail.i_like = 0;
				detail.likes--;
			}
			$http.get(urls.API+"/page/"+detail.id+"/"+type);
		}
		else
		{
			$scope.withoutLogin = true;
		}
	}
}])