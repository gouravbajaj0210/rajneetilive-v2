.controller('partiesMainCtrl', ['$scope','Page','urls','Auth','$mdToast','$localStorage','Toast','$filter', function($scope, Page,urls,Auth,$mdToast,$localStorage,Toast,$filter){
	Page.setTitle('Parties');
	$scope.IMAGE_PATH = urls.IMAGE_PATH;
	$scope.loginCheck = Auth.loggedIn();

	function successAuth(res) {
	    $localStorage.token = res.token;
	    $localStorage.authId = res.id;
	    Toast.queue($filter('translate')('LOGIN_MSG'));
	   // $state.go('user.timeline.public');
	   window.location="/";
	   $state.reload();
	}


	$scope.login = function (mobile, password) {
		 
		$scope.loginLoading=true;

	    var formData = {
	        mobile: mobile,
	        password: password
	    };


	    Auth.login(formData, successAuth, function (resp) {
	    	$scope.loginLoading=false;
	        $mdToast.show(
	          $mdToast.simple()
	          .content(resp.message)
	          .position('top left')
	          .hideDelay(3000)
	        );
	    })
	};
}])