.factory('Assemblies', function ($resource,urls) {

    return $resource(urls.API + '/assembly',{}, {
		'get': {url: urls.API + '/assembly/:state', params: {state: '@state'}, method:'GET', cache: true, format: 'json'}
	});
})