.factory('signupModal', function(){

    var status=0;
    return{
        show: function() {status=1},
        hide: function() {status=0},
        get: function(){return status}
        
    };
})

.factory('UserSignup', function($http, urls){
	return{
		create: function (data, success, error) {
            $http.post(urls.API + '/users/signup', data).success(success).error(error)
        },
        getTokenClaims: function () {
            return tokenClaims;
        },
        loggedIn: function(){return tokenClaims.sub ? true : false},
    }

})