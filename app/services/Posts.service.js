.factory('Posts', function ($resource,urls) {

    return $resource(urls.API + '/posts',{}, {
	'get': { url: urls.API + '/posts/:type',params: {type: '@type'}, method:'GET', cache: false, format: 'json'},
	'profile': { url: urls.API + '/posts/profile/:slug',params: {slug: '@slug'}, method:'GET', cache: false, format: 'json'},
	'like': { url: urls.API + '/posts/:id/:type',params: {id: '@id',type: '@type'}, method:'GET', cache: false, format: 'json'},
	'comments': { url: urls.API + '/posts/:id/comments',params: {id: '@id'}, method:'GET', cache: false, format: 'json'},
	'addComment': { url: urls.API + '/posts/:id/comments',params: {id: '@id'}, method:'POST', cache: false, format: 'json'},
	'query': { method:'GET', cache: false, isArray:true, format: 'json'},
	'delete':{ url:urls.API + '/posts/:id/delete', params: {id:'@id'}, method:'GET', cache:false, format:'json'},
	'share':{url:urls.API+ '/posts/:postId/share/:userId', params:{postId:'@postId', userId:'@userId'}, method:'GET', cache:false, format: 'json'}
	});
})