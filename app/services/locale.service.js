.factory('locale', function($translate,$localStorage){

    return{
        setLocale: function(locale){
            $localStorage.locale=locale;
            $translate.use(locale);
        },
        getLocale: function(){
            return $localStorage.locale ? $localStorage.locale : 'EN' ;
        }
    }

})