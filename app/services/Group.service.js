.factory('Group', function($http, urls){
	return{
		create: function (data, success, error) {
            $http.post(urls.API + '/group/create', data).success(success).error(error)
        },

        update: function (data, success){
        	$http.post(urls.API + '/group/update', data).success(success)
        }
    }

})
.factory('singleGroup', function ($resource,urls) {

    return $resource(urls.API + '/group',{}, {
    	'check':{url:urls.API + '/group/authCheck/:user', params: {user:'@user'}, method:'GET', cache:false, format:'json'},
		'get': { url: urls.API + '/group/:slug',params: {slug: '@slug'}, method:'GET', cache: false, format: 'json'},
		'allUser':{url: urls.API + '/users/all/:slug',params:{slug:'@slug'}, method:'GET', cache:false, format:'json'},
		'Add':{url: urls.API + '/users/:type/:group/:user',params:{type:'@type', group:'@group', user:'@user'}, method:'GET', cache:false, format:'json'},
		'delete':{url:urls.API+'/group/delete/:id', params:{id:'@id', method:'GET', cache:false,format:'json'}},
		'left':{url:urls.API+'/group/left/:id/:groupID', params:{id:'@id', groupID:'@groupID', method:'GET', cache:false, format:'json'}}
	});
})