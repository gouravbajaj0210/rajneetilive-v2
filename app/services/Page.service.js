.factory('Page', function($filter) {
   var title = '';
   return {
     title: function() { return title },
     setTitle: function(newTitle) { title = newTitle + " - " + $filter('translate')('RAJNEETI_TITLE') }
   };
})