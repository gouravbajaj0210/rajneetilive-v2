.factory('nextCmAssembly', function ($resource,urls) {

    return $resource(urls.NEXTCM_API + '/assembly',{}, {
		'get': { method:'GET', cache: true, format: 'json'},
		'village': { url: urls.NEXTCM_API + '/assembly/:type',params: {type: '@type'}, method:'GET', cache: false, format: 'json'},
		'cm': { url: urls.NEXTCM_API + '/cm/:type',params: {type: '@type'}, method:'GET', cache: false, format: 'json'}
	});
})