.factory('Toast',['$mdToast','$localStorage', function($mdToast,$localStorage){

    return{
        has: function(){
            if($localStorage.toast)
                return $localStorage.toast.show;
            else
                return false;
        },
        show: function(content) {
            var toast= $localStorage.toast;

            return $mdToast.show(
                $mdToast.simple()
                .content(toast.content)
                .position(toast.position)
                // .action(toast.action)
                .hideDelay(toast.delay)
                );
        },
        queue: function(setContent,setDelay,setPosition){
            var setDelay = typeof setDelay !== 'undefined' ? setDelay : 3000;
            var setPosition = typeof setPosition !== 'undefined' ? setPosition : 'bottom left';

            $localStorage.toast={show:true,content:setContent,delay:setDelay,position:setPosition};
        },
        clear: function(){
            delete $localStorage.toast;

        }
    }
}])