.factory('Auth', ['$http','$localStorage','urls','$window','$state','$rootScope','Toast','$filter', function ($http, $localStorage, urls,$window,$state,$rootScope,Toast,$filter) {
            function urlBase64Decode(str) {
                var output = str.replace('-', '+').replace('_', '/');
                switch (output.length % 4) {
                    case 0:
                        break;
                    case 2:
                        output += '==';
                        break;
                    case 3:
                        output += '=';
                        break;
                    default:
                        throw 'Illegal base64url string!';
                }
                return window.atob(output);
            }

            function getClaimsFromToken() {
                try{
                    var token = $localStorage.token;
                    var user = {};
                    if (typeof token !== 'undefined') {
                        var encoded = token.split('.')[1];
                        user = JSON.parse(urlBase64Decode(encoded));
                    }
                    else{
                        return false;
                    }
                    return user;
                }
                catch(error){
                    return false;
                }
            }

            var tokenClaims = getClaimsFromToken();

            return {
                // signup: function (data, success, error) {
                //     $http.post(urls.BASE + '/signup', data).success(success).error(error)
                // },
                login: function (data, success, error) {
                    $http.post(urls.API + '/users/login', data).success(success).error(error)
                },
                logout: function (success) {
                    $http.post(urls.API + '/users/logout').then(function(){
                        tokenClaims = {};
                        delete $localStorage.token;
                        delete $localStorage.authId;
                        delete $localStorage.popup;
                        
                        
                        Toast.queue($filter('translate')('LOGOUT_MSG'),3000,'bottom left');
                        window.location='/';
                        // $rootScope.$state=$state.go('home',{}, {reload: true});

  
                    });
                    
                },
                access: function(success, error){
                    $http.post(urls.API + '/access').success(success).error(error)
                },
                getTokenClaims: function () {
                    return tokenClaims;
                },
                loggedIn: function(){return tokenClaims.sub ? true : false},

                refreshToken: function(success){
                    $http.post(urls.API + '/refresh-token').success(success)
                },
                online:function(){
                    $http.get(urls.API+'/user/online')
                },
                offline:function(){
                    $http.get(urls.API+'/user/offline');
                }
            };
        }
])