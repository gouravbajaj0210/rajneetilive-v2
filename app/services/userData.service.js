.factory('userData', function($http,$q,urls,$timeout) {
    var cache;

    function getTheData() {

        var d = $q.defer();
        if( cache ) {
            d.resolve(cache);
        }
        else {

            $timeout(
                function(){

            $http.post(urls.API + '/access').then(
                function success(response) {
                    cache = response.data;
                    d.resolve(cache);
                },
                function failure(reason) {
                    d.reject(reason);
                }
            );

        },5000);
        }


        return d.promise;
    }

    function clearCache() {
        cache = null;
    }

    return {
        getData: function(){return getTheData()},
        clearCache: clearCache
    };
})