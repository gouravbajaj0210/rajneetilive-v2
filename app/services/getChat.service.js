.factory('getChat', function($resource, urls){
	 return $resource(urls.API + '/GetChat',{}, {
		'get': { url: urls.API + '/GetChat/:sender/:receive',params: {sender: '@sender', receive:'@receive'}, method:'GET', cache: false, format: 'json'}	
	});
})
