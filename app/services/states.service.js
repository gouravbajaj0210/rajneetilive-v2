.factory('States', function ($resource,urls) {

    return $resource(urls.API + '/states',{}, {
		'get': { method:'GET', cache: true, format: 'json'}
	});
})