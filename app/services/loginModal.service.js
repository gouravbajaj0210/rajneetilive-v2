.factory('loginModal', function(){

    var status=0;
    return{
        show: function() {status=1},
        hide: function() {status=0},
        get: function(){return status}
        
    };
})