.factory('User', function ($resource,urls) {

    return $resource(urls.API + '/users',{}, {
	'get': { method:'GET', cache: true, format: 'json'},
	'query': { method:'GET', cache: true, isArray:true, format: 'json' },
	'onlineUsers':{method:'GET', url: urls.API + '/users/onlineUsers', cache:true, format:'json'}
	});
})