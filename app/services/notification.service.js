.factory('Notification', function ($resource,urls, $http) {
	return {
		notification: function(){
			return $http.get(urls.API+'/notification').success(function(response){
				return response.notification;
			});
		}
	};
})

.factory('NotificationGet', function($http, urls){
	return {
		getNotification: function(){
			return $http.get(urls.API+'/notification/data').success(function(response){
				return response.notification;
			});


		},

		View: function(){
			return $http.get(urls.API+'/notification/view').success(function(response){
				return response.message;
			});
		}
	};


})

