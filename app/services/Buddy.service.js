.factory('BuddyUp', function ($resource,urls) {
    return $resource(urls.API + '/users/buddy-up',{}, {
		'send': { url: urls.API + '/users/buddy-up/:id',params: {id: '@id'}, method:'GET', cache: false, format: 'json'}
	});
})


.factory('BuddyRequest', function($http, urls){
	return {
		getRequest: function(){
			return $http.get(urls.API+'/buddy/request/get').success(function(response){
				return response.notification;
			});


		},

		view: function(){
			return $http.get(urls.API+'/buddy/request/view').success(function(response){
				return response.message;
			});
		},

		accept:function(id){
			return $http.get(urls.API+'/buddy/accept/'+id).success(function(response){
				return response;
			});
		}
	};
})
