.factory('Villages', function ($resource,urls) {

    return $resource(urls.API + '/village',{}, {
		'get': { url: urls.API + '/village/:type',params: {type: '@type'}, method:'GET', cache: false, format: 'json'}	
	});
})