.factory('Parties', function ($resource,urls) {
    return $resource(urls.API + '/parties',{}, {
		'get': { method:'GET', cache: true, format: 'json'}
	});
})