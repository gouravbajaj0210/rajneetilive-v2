.factory('UpdateInfo', function($http, urls){
	return{
		change: function (data, success) {
            $http.post(urls.API + '/users/update/'+data.type, data).success(success);
        },
        userName: function(data, success){
        	$http.post(urls.API + '/users/'+data.type+'/check', data).success(success);
        },
        getTokenClaims: function () {
            return tokenClaims;
        },
        loggedIn: function(){return tokenClaims.sub ? true : false},
    }

})