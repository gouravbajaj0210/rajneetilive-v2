
.config(function($stateProvider,$urlRouterProvider,$locationProvider) {
              // Send to login if the URL was not found
  
    $urlRouterProvider.otherwise("/");
    
    $locationProvider.html5Mode(true);
    
    $stateProvider
    
    .state("guest", {
      authenticate: false,
      abstract: true,
      url:"",
      template:"<ui-view></ui-view>"

    })
    .state("guest.default", {
      authenticate: false,
      url:"",
      views: {
            '': { 
                  templateUrl: '/pages/guest/guest.main.view.html',
                  controller: "guestCtrl",
                },

            // Home container
            'homeContainer@guest.default': { 
                templateUrl: '/pages/guest/partials/home-container.view.html',
                controller: 'homeContainerCtrl'
            },
            // footer
            'footer@guest.default': { 
                templateUrl: '/pages/guest/partials/footer.view.html',
                controller: 'footerCtrl'
            },
            // Login modal
            'loginModal@guest.default': { 
                templateUrl: '/pages/guest/partials/login-modal.view.html',
                controller: 'loginCtrl'
            },
            // Signup modal
            'signupModal@guest.default': { 
                templateUrl: '/pages/guest/partials/signup-modal.view.html',
                controller: 'signupCtrl'
            }
        }
    })
  .state("info", {
     url:"",
      views:{
        '':{
             templateUrl: '/pages/user/user.view.html',
              controller: 'userCtrl'
        },
         'footer@info':{
              templateUrl:'/pages/user/partials/footer.view.html',
              controller:'footerCtrl'
            }
      }
  })

  .state("info.otp", {
      authenticate:true,
      url:"/verify",
      views:{
          '':{
            templateUrl:'/pages/user/userInfo/otp.view.html',
            controller:'otpCtrl',
            cache: false
          },
          'header@info.otp':{
              templateUrl:'/pages/user/userInfo/partials/header.view.html',
              controller:'infoHeaderCtrl'
          }
      }
  })

  .state("user", {
      authenticate: true,
      abstract: true,
      url:"",
      views: {
            '': { 
                  templateUrl: '/pages/user/user.view.html',
                  controller: 'userCtrl'
                 },
            'fabDial@user': { 
                templateUrl: '/pages/user/partials/fab-dial.view.html',
                controller: 'fabDialCtrl'
            },
            'navbar@user': { 
                templateUrl: '/pages/user/partials/navbar.view.html',
                controller: 'navbarCtrl'
            },
            'footer@user':{
              templateUrl:'/pages/user/partials/footer.view.html',
              controller:'footerCtrl'
            }
            
          }
    })
    .state("user.timeline", {
      url: "",
      authenticate: true,
      abstract: true,
      views: {
            '': { 
                  templateUrl: '/pages/user/timeline/timeline.main.view.html',
                  controller: "timelineCtrl",
                  cache: false
                },
            'leftSide@user.timeline':{
              templateUrl:'/pages/user/partials/left-Sidebar.view.html',
              controller:"leftSideCtrl",
              cache:false
            },
            'rightSide@user.timeline':{
              templateUrl:'/pages/user/partials/right-Sidebar.view.html',
              controller:"rightSide",
              cache:false
            },
            'tabs@user.timeline': { 
                templateUrl: '/pages/user/partials/tabs.view.html',
                controller: 'tabsCtrl',
                cache: false
            }

            
        }
      
    })
    .state("user.timeline.public", {
      url: "/",
      authenticate: true,
      views: {
            '': { 
                  templateUrl: '/pages/user/timeline/posts.view.html',
                  controller: "timelinePublicCtrl",
                  cache: false
                }
              }
      
    })
    .state("user.timeline.party", {
      url: "/party",
      authenticate: true,
      views: {
            '': { 
                  templateUrl: '/pages/user/timeline/posts.view.html',
                  controller: "timelinePartyCtrl",
                  cache: false
                }
              }
      
    })
    .state("user.timeline.assembly", {
      url: "/assembly",
      authenticate: true,
      views: {
            '': { 
                  templateUrl: '/pages/user/timeline/posts.view.html',
                  controller: "timelineAssemblyCtrl",
                  cache: false
                }
              }
      
    })

    .state("user.buddy", {
      url: "",
      authenticate: true,
      abstract: true,
      views: {
            '': { 
                  templateUrl: '/pages/user/buddy/buddyUpAll.main.view.html',
                  controller: "buddyUpCtrl",
                  cache: false
                },
            'leftSide@user.buddy':{
              templateUrl:'/pages/user/partials/left-Sidebar.view.html',
              controller:"leftSideCtrl",
              cache:false
            },
            'rightSide@user.buddy':{
              templateUrl:'/pages/user/partials/right-Sidebar.view.html',
              controller:"rightSide",
              cache:false
            }

            
        }
      
    })
    .state("user.buddy.allBuddy", {
      url: "/buddy",
      authenticate: true,
      views: {
            '': { 
                  templateUrl: '/pages/user/buddy/buddyUp.view.html',
                  controller: "buddyUpAllCtrl",
                  cache: false
                }
              }
      
    })

    .state("user.profile", {
      url: "/profile/:slug",
      authenticate: true,
      views: {
            '': { 
                  templateUrl: '/pages/user/profile/profile.view.html',
                  controller: "profileCtrl"
                }
              },
            'rightSide@user.profile':{
              templateUrl:'/pages/user/partials/right-Sidebar.view.html',
              controller:"rightSide",
              cache:false
            }  
      
    })

     .state("user.opinion", {
      url: "",
      authenticate: true,
      abstract: true,
      views: {
            '': { 
                  templateUrl: '/pages/user/opinion/opinion.main.view.html',
                  controller: "timelineCtrl",
                  cache: false
                },
              'rightSidebar@user.opinion':{
                  templateUrl:'/pages/user/partials/right-Sidebar.view.html',
                  controller:'rightSide'
              }
        }
      
    })
    .state("user.opinion.public", {
      url:"/opinion",
      authenticate:true,
      views: {
          '':{
            templateUrl:'/pages/user/opinion/opinion.view.html',
            controller: "opinionCtrl"
          }
        }
     })

    .state("user.opinion.nextcm", {
      url:"/nextcm",
      authenticate:true,
      views: {
          '':{
            templateUrl:'/pages/user/opinion/nextcm.view.html',
            controller: "nextcmCtrl"
          }
        }
     })

    .state("user.opinion.nextmla", {
      url:"/nextmla",
      authenticate:true,
      views: {
          '':{
            templateUrl:'/pages/user/opinion/nextmla.view.html',
            controller: "nextmlaCtrl"
          }
        }
     })

    .state("user.opinion.issue", {
      url:"/issue",
      authenticate:true,
      views:{
          '':{
            templateUrl:'/pages/user/opinion/issue.view.html',
            controller:"issueCtrl"
          }
      }
    })

    .state("user.publicOpinion", {
      url:"/public-opinion",
      authenticate:true,
      views:{
        '':{
          templateUrl:'/pages/user/opinion/publicOpinion.view.html',
          controller:"publicOpinionCtrl"
        }
      }
    })

    .state("user.question", {
      url:'/unanswered/:id',
      authenticate:true,
      views:{
        '':{
          templateUrl:'/pages/user/opinion/question.view.html',
          controller:'questionCtrl'
        }
      }
    })

    .state("user.answared", {
      url:'/answered/:id',
      authenticate:true,
      views:{
        '':{
          templateUrl:'/pages/user/opinion/answered.view.html',
          controller:'answeredCtrl'
        }
      }
    })

     .state("user.messages", {
      url: "",
      authenticate: true,
      abstract: true,
      views: {
            '': { 
                  templateUrl: '/pages/user/message/messageMain.view.html',
                  controller: "MessageMainCtrl",
                  cache: false
            },

          'rightSide@user.messages':{
              templateUrl:'/pages/user/partials/right-Sidebar.view.html',
              controller:"rightSide",
              cache:false
            }  
        }
    })
     .state('user.messages.msg', {
      url:"/messages",
      authenticate:true,
      views:{
        '':{
            templateUrl:'/pages/user/message/message.view.html',
            controller:"MessageCtrl",
        }
      }
    })

     .state('user.messages.closeGroup', {
        url:"/group/:group",
        authenticate:true,
        views:{
          '':{
              templateUrl:'/pages/user/message/closeGroup.view.html',
              controller:"closeGroupCtrl"
          }
        }
     })

     .state('user.messages.groups', {
        url:"/groups",
        authenticate:true,
        views:{
          '':{
              templateUrl:'/pages/user/message/groupList.view.html',
              controller:"groupListCtrl"
          }
        }
     })

    // account setting state
    .state("user.setting", {
      url: "",
      authenticate: true,
      abstract: true,
      views: {
            '': { 
                  templateUrl: '/pages/user/setting/settingMain.view.html',
                  controller: "settingCtrl",
                  cache: false
                }      
        }
      
    })
    .state("user.setting.general", {
      url:"/setting/general",
      authenticate:true,
      views:{
          '':{
              templateUrl:'/pages/user/setting/general.view.html',
              controller:"generalCtrl",
          }
      }
    })
    .state('user.setting.location', {
      url:"/setting/assembly-party",
      authenticate:true,
      views:{
        '':{
            templateUrl:'/pages/user/setting/location.view.html',
            controller:"locationCtrl",
        }
      }
    })
    // account setting state

    // party pages
    .state("parties", {
      url: "",
      authenticate:false,
      views: {
            '': { 
                  templateUrl: '/pages/user/parties/parties.main.html',
                  controller: "partiesMainCtrl",
                  cache: false
                },
            'navbar@parties': { 
                templateUrl: '/pages/user/partials/navbar.view.html',
                controller: 'navbarCtrl'
            }
                
        }
    })
    
    .state('parties.page', {
      url:"/party/:party",
      authenticate:false,
      views:{
        '':{
            templateUrl:'/pages/user/parties/partyPage.view.html',
            controller:'partyPageCtrl'
        }
      }
    })
    .state("user.news", {
      url: "/news",
      authenticate: true,
      views: {
            '': { 
                  templateUrl: '/pages/user/news/news.view.html',
                  controller: "newsCtrl"
                }
              }
      
    });

})