/*** Main app controller ***/
.controller('mainCtrl', ['$scope','Page','loginModal','signupModal','Toast','$mdToast','$timeout','locale','urls','User','$state', function($scope,Page,loginModal,signupModal,Toast,$mdToast,$timeout,locale,urls,User,$state){

  $scope.Page=Page;
  console.log('%c STOP! ', ' color: #f55;font-size:50px;text-shadow: 2px 0 0 #000, -2px 0 0 #000, 0 2px 0 #000, 0 -2px 0 #000, 1px 1px #000, -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000;');
  $scope.IMAGE_PATH= urls.IMAGE_PATH;

  locale.setLocale(locale.getLocale());

  $scope.signupModalStatus= function(){return signupModal.get()};
  $scope.loginModalStatus= function(){return loginModal.get()};

  $scope.closeAllModals= function(){
      signupModal.hide();
      loginModal.hide();
  }
// User.get({},function(user){
//   console.log(user);
//         $scope.user= user.data;  
//        if(user.data.verify == '1')
//        {
//             $state.go('user.timeline.public');
//        }
//        else{
//           $state.go('info.otp');
//        }
//     });
  

if(Toast.has()){

  $timeout(function() {
    Toast.show();
    Toast.clear();
    }, 10);
}


  // angular.element(document).on('click', function (e) {
  //   console.log(e);

  //   if(!(e.target).is('.search-input-con .search-results-con a') && !(e.target).is('.my-search-input input'))
  //   loginModal.hide();
  // });


}])