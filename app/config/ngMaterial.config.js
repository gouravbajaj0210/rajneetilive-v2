.config(function($mdThemingProvider) {

    $mdThemingProvider.theme('default')
      .primaryPalette('blue', {
                'default': '500',
                'hue-2': '500'
            });

})