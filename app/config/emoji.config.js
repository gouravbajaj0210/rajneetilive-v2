.config(function(emojiConfigProvider) {
    emojiConfigProvider.addAlias("smile", ":)");
    emojiConfigProvider.addAlias("heart", "<3");
    emojiConfigProvider.addAlias("ok_hand", "+1");
    
  })