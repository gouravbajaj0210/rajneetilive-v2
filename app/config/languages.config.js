.config(function($translateProvider) {

$translateProvider.useStaticFilesLoader({
    prefix: '/locales/locale-',
    suffix: '.json'
});

$translateProvider.preferredLanguage('EN');

$translateProvider.fallbackLanguage('EN');

$translateProvider.useSanitizeValueStrategy(null);

})