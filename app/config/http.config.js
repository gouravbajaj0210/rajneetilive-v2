.config(function($httpProvider) {
    
    $httpProvider.interceptors.push(['$q', '$window', '$localStorage', function ($q, $window, $localStorage) {
                
                return {
                    'request': function (config) {
                        config.headers = config.headers || {};
                        if ($localStorage.token) {
                            config.headers.Authorization = 'Bearer ' + $localStorage.token;
                        }
                        return config;
                    },
                    'response': function (response) {

                        if (typeof response.headers('new-token') != 'undefined' && response.headers('new-token') != null ){
                           
                            $localStorage.token=response.headers('new-token');
                        }

                        return response;

                    },
                    'responseError': function (response){

                        // console.log('in error',response.headers(),response.headers('vary'), response.headers('new-token'),'in error');
                        // if (typeof response.headers('Authorization') != 'undefined' && response.headers('Authorization') != null ){
                        //  console.log(response.headers('Authorization'));
                        //  $localStorage.token=response.headers('Authorization');
                        // }

                        if (response.status === 401 || response.status === 403 || response.data.error==='token_invalid') {

                            
                            delete $localStorage.token;
                            $window.location.reload();
                            
                        }

                        return $q.reject(response);

                    }                
                };
            }]);  
    
  })
