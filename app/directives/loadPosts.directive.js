
    .directive('loadPosts', function ($window,$state) {
        return {
    
            link: function (scope, element, attr) {


                if ((element[0].y - 200) < $window.innerHeight) {
                    scope.loadMorePosts(attr.loadPosts);
                }
                angular.element($window).on('scroll', function () {
                    var stateName="user.timeline."+attr.loadPosts;
                    if ( ((element[0].getBoundingClientRect().top - 200) < $window.innerHeight) && !scope.postsLoading && attr.loadPosts == scope.postType && stateName == $state.current.name) {

                        scope.postsLoading=true;

                        scope.loadMorePosts().then(function(){
                            scope.postsLoading=false;
                        });

                    }
                });

              
            }
        };
    })
