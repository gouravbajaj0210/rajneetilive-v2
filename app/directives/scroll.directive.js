.directive("scroll", function ($window) {
    return function(scope, element, attrs) {
        var offset = scope.$eval(attrs.scrollOffset);
        angular.element($window).bind("scroll", function() {
             if (this.pageYOffset >= offset) {
                 scope.boolChangeClass = true;
             } else {
                 scope.boolChangeClass = false;
             }
            scope.$apply();
        });
    };
})