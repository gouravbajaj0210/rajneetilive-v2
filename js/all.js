angular.module('myApp',[
	'ngMaterial',
	'ngResource',
	'ngMessages',
	'ngAria',
	'ngStorage',
	'ngMdIcons',
	'ui.router',
	'pascalprecht.translate',
	'relativeDate',
	'ngSanitize',
	'com.2fdevs.videogular',
	'com.2fdevs.videogular.plugins.controls',
	'ngFileUpload',
	'luegg.directives',
	'btford.socket-io',
	'dbaq.emoji',
	'hm.readmore'
])
/*** Main app controller ***/
.controller('mainCtrl', ['$scope','Page','loginModal','signupModal','Toast','$mdToast','$timeout','locale','urls','User','$state', function($scope,Page,loginModal,signupModal,Toast,$mdToast,$timeout,locale,urls,User,$state){

  $scope.Page=Page;
  console.log('%c STOP! ', ' color: #f55;font-size:50px;text-shadow: 2px 0 0 #000, -2px 0 0 #000, 0 2px 0 #000, 0 -2px 0 #000, 1px 1px #000, -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000;');
  $scope.IMAGE_PATH= urls.IMAGE_PATH;

  locale.setLocale(locale.getLocale());

  $scope.signupModalStatus= function(){return signupModal.get()};
  $scope.loginModalStatus= function(){return loginModal.get()};

  $scope.closeAllModals= function(){
      signupModal.hide();
      loginModal.hide();
  }
// User.get({},function(user){
//   console.log(user);
//         $scope.user= user.data;  
//        if(user.data.verify == '1')
//        {
//             $state.go('user.timeline.public');
//        }
//        else{
//           $state.go('info.otp');
//        }
//     });
  

if(Toast.has()){

  $timeout(function() {
    Toast.show();
    Toast.clear();
    }, 10);
}


  // angular.element(document).on('click', function (e) {
  //   console.log(e);

  //   if(!(e.target).is('.search-input-con .search-results-con a') && !(e.target).is('.my-search-input input'))
  //   loginModal.hide();
  // });


}])

.config(function($stateProvider,$urlRouterProvider,$locationProvider) {
              // Send to login if the URL was not found
  
    $urlRouterProvider.otherwise("/");
    
    $locationProvider.html5Mode(true);
    
    $stateProvider
    
    .state("guest", {
      authenticate: false,
      abstract: true,
      url:"",
      template:"<ui-view></ui-view>"

    })
    .state("guest.default", {
      authenticate: false,
      url:"",
      views: {
            '': { 
                  templateUrl: '/pages/guest/guest.main.view.html',
                  controller: "guestCtrl",
                },

            // Home container
            'homeContainer@guest.default': { 
                templateUrl: '/pages/guest/partials/home-container.view.html',
                controller: 'homeContainerCtrl'
            },
            // footer
            'footer@guest.default': { 
                templateUrl: '/pages/guest/partials/footer.view.html',
                controller: 'footerCtrl'
            },
            // Login modal
            'loginModal@guest.default': { 
                templateUrl: '/pages/guest/partials/login-modal.view.html',
                controller: 'loginCtrl'
            },
            // Signup modal
            'signupModal@guest.default': { 
                templateUrl: '/pages/guest/partials/signup-modal.view.html',
                controller: 'signupCtrl'
            }
        }
    })
  .state("info", {
     url:"",
      views:{
        '':{
             templateUrl: '/pages/user/user.view.html',
              controller: 'userCtrl'
        },
         'footer@info':{
              templateUrl:'/pages/user/partials/footer.view.html',
              controller:'footerCtrl'
            }
      }
  })

  .state("info.otp", {
      authenticate:true,
      url:"/verify",
      views:{
          '':{
            templateUrl:'/pages/user/userInfo/otp.view.html',
            controller:'otpCtrl',
            cache: false
          },
          'header@info.otp':{
              templateUrl:'/pages/user/userInfo/partials/header.view.html',
              controller:'infoHeaderCtrl'
          }
      }
  })

  .state("user", {
      authenticate: true,
      abstract: true,
      url:"",
      views: {
            '': { 
                  templateUrl: '/pages/user/user.view.html',
                  controller: 'userCtrl'
                 },
            'fabDial@user': { 
                templateUrl: '/pages/user/partials/fab-dial.view.html',
                controller: 'fabDialCtrl'
            },
            'navbar@user': { 
                templateUrl: '/pages/user/partials/navbar.view.html',
                controller: 'navbarCtrl'
            },
            'footer@user':{
              templateUrl:'/pages/user/partials/footer.view.html',
              controller:'footerCtrl'
            }
            
          }
    })
    .state("user.timeline", {
      url: "",
      authenticate: true,
      abstract: true,
      views: {
            '': { 
                  templateUrl: '/pages/user/timeline/timeline.main.view.html',
                  controller: "timelineCtrl",
                  cache: false
                },
            'leftSide@user.timeline':{
              templateUrl:'/pages/user/partials/left-Sidebar.view.html',
              controller:"leftSideCtrl",
              cache:false
            },
            'rightSide@user.timeline':{
              templateUrl:'/pages/user/partials/right-Sidebar.view.html',
              controller:"rightSide",
              cache:false
            },
            'tabs@user.timeline': { 
                templateUrl: '/pages/user/partials/tabs.view.html',
                controller: 'tabsCtrl',
                cache: false
            }

            
        }
      
    })
    .state("user.timeline.public", {
      url: "/",
      authenticate: true,
      views: {
            '': { 
                  templateUrl: '/pages/user/timeline/posts.view.html',
                  controller: "timelinePublicCtrl",
                  cache: false
                }
              }
      
    })
    .state("user.timeline.party", {
      url: "/party",
      authenticate: true,
      views: {
            '': { 
                  templateUrl: '/pages/user/timeline/posts.view.html',
                  controller: "timelinePartyCtrl",
                  cache: false
                }
              }
      
    })
    .state("user.timeline.assembly", {
      url: "/assembly",
      authenticate: true,
      views: {
            '': { 
                  templateUrl: '/pages/user/timeline/posts.view.html',
                  controller: "timelineAssemblyCtrl",
                  cache: false
                }
              }
      
    })

    .state("user.buddy", {
      url: "",
      authenticate: true,
      abstract: true,
      views: {
            '': { 
                  templateUrl: '/pages/user/buddy/buddyUpAll.main.view.html',
                  controller: "buddyUpCtrl",
                  cache: false
                },
            'leftSide@user.buddy':{
              templateUrl:'/pages/user/partials/left-Sidebar.view.html',
              controller:"leftSideCtrl",
              cache:false
            },
            'rightSide@user.buddy':{
              templateUrl:'/pages/user/partials/right-Sidebar.view.html',
              controller:"rightSide",
              cache:false
            }

            
        }
      
    })
    .state("user.buddy.allBuddy", {
      url: "/buddy",
      authenticate: true,
      views: {
            '': { 
                  templateUrl: '/pages/user/buddy/buddyUp.view.html',
                  controller: "buddyUpAllCtrl",
                  cache: false
                }
              }
      
    })

    .state("user.profile", {
      url: "/profile/:slug",
      authenticate: true,
      views: {
            '': { 
                  templateUrl: '/pages/user/profile/profile.view.html',
                  controller: "profileCtrl"
                }
              },
            'rightSide@user.profile':{
              templateUrl:'/pages/user/partials/right-Sidebar.view.html',
              controller:"rightSide",
              cache:false
            }  
      
    })

     .state("user.opinion", {
      url: "",
      authenticate: true,
      abstract: true,
      views: {
            '': { 
                  templateUrl: '/pages/user/opinion/opinion.main.view.html',
                  controller: "timelineCtrl",
                  cache: false
                },
              'rightSidebar@user.opinion':{
                  templateUrl:'/pages/user/partials/right-Sidebar.view.html',
                  controller:'rightSide'
              }
        }
      
    })
    .state("user.opinion.public", {
      url:"/opinion",
      authenticate:true,
      views: {
          '':{
            templateUrl:'/pages/user/opinion/opinion.view.html',
            controller: "opinionCtrl"
          }
        }
     })

    .state("user.opinion.nextcm", {
      url:"/nextcm",
      authenticate:true,
      views: {
          '':{
            templateUrl:'/pages/user/opinion/nextcm.view.html',
            controller: "nextcmCtrl"
          }
        }
     })

    .state("user.opinion.nextmla", {
      url:"/nextmla",
      authenticate:true,
      views: {
          '':{
            templateUrl:'/pages/user/opinion/nextmla.view.html',
            controller: "nextmlaCtrl"
          }
        }
     })

    .state("user.opinion.issue", {
      url:"/issue",
      authenticate:true,
      views:{
          '':{
            templateUrl:'/pages/user/opinion/issue.view.html',
            controller:"issueCtrl"
          }
      }
    })

    .state("user.publicOpinion", {
      url:"/public-opinion",
      authenticate:true,
      views:{
        '':{
          templateUrl:'/pages/user/opinion/publicOpinion.view.html',
          controller:"publicOpinionCtrl"
        }
      }
    })

    .state("user.question", {
      url:'/unanswered/:id',
      authenticate:true,
      views:{
        '':{
          templateUrl:'/pages/user/opinion/question.view.html',
          controller:'questionCtrl'
        }
      }
    })

    .state("user.answared", {
      url:'/answered/:id',
      authenticate:true,
      views:{
        '':{
          templateUrl:'/pages/user/opinion/answered.view.html',
          controller:'answeredCtrl'
        }
      }
    })

     .state("user.messages", {
      url: "",
      authenticate: true,
      abstract: true,
      views: {
            '': { 
                  templateUrl: '/pages/user/message/messageMain.view.html',
                  controller: "MessageMainCtrl",
                  cache: false
            },

          'rightSide@user.messages':{
              templateUrl:'/pages/user/partials/right-Sidebar.view.html',
              controller:"rightSide",
              cache:false
            }  
        }
    })
     .state('user.messages.msg', {
      url:"/messages",
      authenticate:true,
      views:{
        '':{
            templateUrl:'/pages/user/message/message.view.html',
            controller:"MessageCtrl",
        }
      }
    })

     .state('user.messages.closeGroup', {
        url:"/group/:group",
        authenticate:true,
        views:{
          '':{
              templateUrl:'/pages/user/message/closeGroup.view.html',
              controller:"closeGroupCtrl"
          }
        }
     })

     .state('user.messages.groups', {
        url:"/groups",
        authenticate:true,
        views:{
          '':{
              templateUrl:'/pages/user/message/groupList.view.html',
              controller:"groupListCtrl"
          }
        }
     })

    // account setting state
    .state("user.setting", {
      url: "",
      authenticate: true,
      abstract: true,
      views: {
            '': { 
                  templateUrl: '/pages/user/setting/settingMain.view.html',
                  controller: "settingCtrl",
                  cache: false
                }      
        }
      
    })
    .state("user.setting.general", {
      url:"/setting/general",
      authenticate:true,
      views:{
          '':{
              templateUrl:'/pages/user/setting/general.view.html',
              controller:"generalCtrl",
          }
      }
    })
    .state('user.setting.location', {
      url:"/setting/assembly-party",
      authenticate:true,
      views:{
        '':{
            templateUrl:'/pages/user/setting/location.view.html',
            controller:"locationCtrl",
        }
      }
    })
    // account setting state

    // party pages
    .state("parties", {
      url: "",
      authenticate:false,
      views: {
            '': { 
                  templateUrl: '/pages/user/parties/parties.main.html',
                  controller: "partiesMainCtrl",
                  cache: false
                },
            'navbar@parties': { 
                templateUrl: '/pages/user/partials/navbar.view.html',
                controller: 'navbarCtrl'
            }
                
        }
    })
    
    .state('parties.page', {
      url:"/party/:party",
      authenticate:false,
      views:{
        '':{
            templateUrl:'/pages/user/parties/partyPage.view.html',
            controller:'partyPageCtrl'
        }
      }
    })
    .state("user.news", {
      url: "/news",
      authenticate: true,
      views: {
            '': { 
                  templateUrl: '/pages/user/news/news.view.html',
                  controller: "newsCtrl"
                }
              }
      
    });

})
.config(function(emojiConfigProvider) {
    emojiConfigProvider.addAlias("smile", ":)");
    emojiConfigProvider.addAlias("heart", "<3");
    emojiConfigProvider.addAlias("ok_hand", "+1");
    
  })
.config(function($httpProvider) {
    
    $httpProvider.interceptors.push(['$q', '$window', '$localStorage', function ($q, $window, $localStorage) {
                
                return {
                    'request': function (config) {
                        config.headers = config.headers || {};
                        if ($localStorage.token) {
                            config.headers.Authorization = 'Bearer ' + $localStorage.token;
                        }
                        return config;
                    },
                    'response': function (response) {

                        if (typeof response.headers('new-token') != 'undefined' && response.headers('new-token') != null ){
                           
                            $localStorage.token=response.headers('new-token');
                        }

                        return response;

                    },
                    'responseError': function (response){

                        // console.log('in error',response.headers(),response.headers('vary'), response.headers('new-token'),'in error');
                        // if (typeof response.headers('Authorization') != 'undefined' && response.headers('Authorization') != null ){
                        //  console.log(response.headers('Authorization'));
                        //  $localStorage.token=response.headers('Authorization');
                        // }

                        if (response.status === 401 || response.status === 403 || response.data.error==='token_invalid') {

                            
                            delete $localStorage.token;
                            $window.location.reload();
                            
                        }

                        return $q.reject(response);

                    }                
                };
            }]);  
    
  })

.config(function($translateProvider) {

$translateProvider.useStaticFilesLoader({
    prefix: '/locales/locale-',
    suffix: '.json'
});

$translateProvider.preferredLanguage('EN');

$translateProvider.fallbackLanguage('EN');

$translateProvider.useSanitizeValueStrategy(null);

})
.config(['$localStorageProvider',
    function ($localStorageProvider) {
        $localStorageProvider.setKeyPrefix('rajneeti-');
}])
.config(function($mdThemingProvider) {

    $mdThemingProvider.theme('default')
      .primaryPalette('blue', {
                'default': '500',
                'hue-2': '500'
            });

})
.constant('urls', {
            API: 'http://api.rajneeti.dev/v1',
            IMAGE_PATH: 'http://api.rajneeti.dev/images',
            NEXTCM_API : 'https://nextcm.com/api'
})

// .constant('urls', {
//             API: 'http://api.rajneetilive.com/v1',
//             IMAGE_PATH: 'http://api.rajneetilive.com/images',
//             NEXTCM_API : 'https://nextcm.com/api'
// })

    .directive('loadPosts', function ($window,$state) {
        return {
    
            link: function (scope, element, attr) {


                if ((element[0].y - 200) < $window.innerHeight) {
                    scope.loadMorePosts(attr.loadPosts);
                }
                angular.element($window).on('scroll', function () {
                    var stateName="user.timeline."+attr.loadPosts;
                    if ( ((element[0].getBoundingClientRect().top - 200) < $window.innerHeight) && !scope.postsLoading && attr.loadPosts == scope.postType && stateName == $state.current.name) {

                        scope.postsLoading=true;

                        scope.loadMorePosts().then(function(){
                            scope.postsLoading=false;
                        });

                    }
                });

              
            }
        };
    })

.directive('pvScrolled', function() {
	return function(scope, elm, attr) {
		var raw = elm[0];

		elm.bind('scroll', function() {
			
		if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight) {
			scope.$apply(attr.pvScrolled);
		}
		});
	};
})


.directive("scroll", function ($window) {
    return function(scope, element, attrs) {
        var offset = scope.$eval(attrs.scrollOffset);
        angular.element($window).bind("scroll", function() {
             if (this.pageYOffset >= offset) {
                 scope.boolChangeClass = true;
             } else {
                 scope.boolChangeClass = false;
             }
            scope.$apply();
        });
    };
})
.directive('scrollWatch', function($timeout) {
  return {
    restrict: 'A',
    link: function(scope, element, attr) {

      scope.$watchCollection(attr.scrollWatch, function(newVal) {
        $timeout(function() {
          element[0].scrollTop = 0;
         // element[0].scrollTop = element[0].scrollHeight;
        });
      });
    }
  }
})
.factory('addPosts', function ($q,$resource,Posts,$state) {

    return {
    	add: function(postType,posts){

    	var deferred = $q.defer();

    	Posts.get({type:postType, 'offset': typeof posts !== 'undefined' ? posts.length : 0 },function(response){
      var newPosts= response.data;

      var postIds= new Array();

      if(typeof posts !== 'undefined'){

        for (var k = 0; k < posts.length; k++) {
          postIds.push(posts[k].id);
        }

      }

  

      for (var i = 0; i < newPosts.length; i++) {
 
      if (postIds.indexOf(newPosts[i].id) == -1){
        console.log('id not found',newPosts[i].id);
        posts.push(newPosts[i]);
      }
     
 
    }

    console.log(posts,'all posts IN END');



    deferred.resolve(posts);


  });
    		

    		return deferred.promise;
    	}
    }
	
})
.factory('Assemblies', function ($resource,urls) {

    return $resource(urls.API + '/assembly',{}, {
		'get': {url: urls.API + '/assembly/:state', params: {state: '@state'}, method:'GET', cache: true, format: 'json'}
	});
})
.factory('Auth', ['$http','$localStorage','urls','$window','$state','$rootScope','Toast','$filter', function ($http, $localStorage, urls,$window,$state,$rootScope,Toast,$filter) {
            function urlBase64Decode(str) {
                var output = str.replace('-', '+').replace('_', '/');
                switch (output.length % 4) {
                    case 0:
                        break;
                    case 2:
                        output += '==';
                        break;
                    case 3:
                        output += '=';
                        break;
                    default:
                        throw 'Illegal base64url string!';
                }
                return window.atob(output);
            }

            function getClaimsFromToken() {
                try{
                    var token = $localStorage.token;
                    var user = {};
                    if (typeof token !== 'undefined') {
                        var encoded = token.split('.')[1];
                        user = JSON.parse(urlBase64Decode(encoded));
                    }
                    else{
                        return false;
                    }
                    return user;
                }
                catch(error){
                    return false;
                }
            }

            var tokenClaims = getClaimsFromToken();

            return {
                // signup: function (data, success, error) {
                //     $http.post(urls.BASE + '/signup', data).success(success).error(error)
                // },
                login: function (data, success, error) {
                    $http.post(urls.API + '/users/login', data).success(success).error(error)
                },
                logout: function (success) {
                    $http.post(urls.API + '/users/logout').then(function(){
                        tokenClaims = {};
                        delete $localStorage.token;
                        delete $localStorage.authId;
                        delete $localStorage.popup;
                        
                        
                        Toast.queue($filter('translate')('LOGOUT_MSG'),3000,'bottom left');
                        window.location='/';
                        // $rootScope.$state=$state.go('home',{}, {reload: true});

  
                    });
                    
                },
                access: function(success, error){
                    $http.post(urls.API + '/access').success(success).error(error)
                },
                getTokenClaims: function () {
                    return tokenClaims;
                },
                loggedIn: function(){return tokenClaims.sub ? true : false},

                refreshToken: function(success){
                    $http.post(urls.API + '/refresh-token').success(success)
                },
                online:function(){
                    $http.get(urls.API+'/user/online')
                },
                offline:function(){
                    $http.get(urls.API+'/user/offline');
                }
            };
        }
])
.factory('BuddyUp', function ($resource,urls) {
    return $resource(urls.API + '/users/buddy-up',{}, {
		'send': { url: urls.API + '/users/buddy-up/:id',params: {id: '@id'}, method:'GET', cache: false, format: 'json'}
	});
})


.factory('BuddyRequest', function($http, urls){
	return {
		getRequest: function(){
			return $http.get(urls.API+'/buddy/request/get').success(function(response){
				return response.notification;
			});


		},

		view: function(){
			return $http.get(urls.API+'/buddy/request/view').success(function(response){
				return response.message;
			});
		},

		accept:function(id){
			return $http.get(urls.API+'/buddy/accept/'+id).success(function(response){
				return response;
			});
		}
	};
})

.factory('chatSocket', ['socketFactory', function(socketFactory){
	return socketFactory();
}])
.factory('getChat', function($resource, urls){
	 return $resource(urls.API + '/GetChat',{}, {
		'get': { url: urls.API + '/GetChat/:sender/:receive',params: {sender: '@sender', receive:'@receive'}, method:'GET', cache: false, format: 'json'}	
	});
})

.factory('Group', function($http, urls){
	return{
		create: function (data, success, error) {
            $http.post(urls.API + '/group/create', data).success(success).error(error)
        },

        update: function (data, success){
        	$http.post(urls.API + '/group/update', data).success(success)
        }
    }

})
.factory('singleGroup', function ($resource,urls) {

    return $resource(urls.API + '/group',{}, {
    	'check':{url:urls.API + '/group/authCheck/:user', params: {user:'@user'}, method:'GET', cache:false, format:'json'},
		'get': { url: urls.API + '/group/:slug',params: {slug: '@slug'}, method:'GET', cache: false, format: 'json'},
		'allUser':{url: urls.API + '/users/all/:slug',params:{slug:'@slug'}, method:'GET', cache:false, format:'json'},
		'Add':{url: urls.API + '/users/:type/:group/:user',params:{type:'@type', group:'@group', user:'@user'}, method:'GET', cache:false, format:'json'},
		'delete':{url:urls.API+'/group/delete/:id', params:{id:'@id', method:'GET', cache:false,format:'json'}},
		'left':{url:urls.API+'/group/left/:id/:groupID', params:{id:'@id', groupID:'@groupID', method:'GET', cache:false, format:'json'}}
	});
})
.factory('locale', function($translate,$localStorage){

    return{
        setLocale: function(locale){
            $localStorage.locale=locale;
            $translate.use(locale);
        },
        getLocale: function(){
            return $localStorage.locale ? $localStorage.locale : 'EN' ;
        }
    }

})
.factory('loginModal', function(){

    var status=0;
    return{
        show: function() {status=1},
        hide: function() {status=0},
        get: function(){return status}
        
    };
})
.factory('nextCmAssembly', function ($resource,urls) {

    return $resource(urls.NEXTCM_API + '/assembly',{}, {
		'get': { method:'GET', cache: true, format: 'json'},
		'village': { url: urls.NEXTCM_API + '/assembly/:type',params: {type: '@type'}, method:'GET', cache: false, format: 'json'},
		'cm': { url: urls.NEXTCM_API + '/cm/:type',params: {type: '@type'}, method:'GET', cache: false, format: 'json'}
	});
})
.factory('Notification', function ($resource,urls, $http) {
	return {
		notification: function(){
			return $http.get(urls.API+'/notification').success(function(response){
				return response.notification;
			});
		}
	};
})

.factory('NotificationGet', function($http, urls){
	return {
		getNotification: function(){
			return $http.get(urls.API+'/notification/data').success(function(response){
				return response.notification;
			});


		},

		View: function(){
			return $http.get(urls.API+'/notification/view').success(function(response){
				return response.message;
			});
		}
	};


})


.factory('Page', function($filter) {
   var title = '';
   return {
     title: function() { return title },
     setTitle: function(newTitle) { title = newTitle + " - " + $filter('translate')('RAJNEETI_TITLE') }
   };
})
.factory('Parties', function ($resource,urls) {
    return $resource(urls.API + '/parties',{}, {
		'get': { method:'GET', cache: true, format: 'json'}
	});
})
.factory('Posts', function ($resource,urls) {

    return $resource(urls.API + '/posts',{}, {
	'get': { url: urls.API + '/posts/:type',params: {type: '@type'}, method:'GET', cache: false, format: 'json'},
	'profile': { url: urls.API + '/posts/profile/:slug',params: {slug: '@slug'}, method:'GET', cache: false, format: 'json'},
	'like': { url: urls.API + '/posts/:id/:type',params: {id: '@id',type: '@type'}, method:'GET', cache: false, format: 'json'},
	'comments': { url: urls.API + '/posts/:id/comments',params: {id: '@id'}, method:'GET', cache: false, format: 'json'},
	'addComment': { url: urls.API + '/posts/:id/comments',params: {id: '@id'}, method:'POST', cache: false, format: 'json'},
	'query': { method:'GET', cache: false, isArray:true, format: 'json'},
	'delete':{ url:urls.API + '/posts/:id/delete', params: {id:'@id'}, method:'GET', cache:false, format:'json'},
	'share':{url:urls.API+ '/posts/:postId/share/:userId', params:{postId:'@postId', userId:'@userId'}, method:'GET', cache:false, format: 'json'}
	});
})
.factory('signupModal', function(){

    var status=0;
    return{
        show: function() {status=1},
        hide: function() {status=0},
        get: function(){return status}
        
    };
})

.factory('UserSignup', function($http, urls){
	return{
		create: function (data, success, error) {
            $http.post(urls.API + '/users/signup', data).success(success).error(error)
        },
        getTokenClaims: function () {
            return tokenClaims;
        },
        loggedIn: function(){return tokenClaims.sub ? true : false},
    }

})
.factory('States', function ($resource,urls) {

    return $resource(urls.API + '/states',{}, {
		'get': { method:'GET', cache: true, format: 'json'}
	});
})
.factory('Toast',['$mdToast','$localStorage', function($mdToast,$localStorage){

    return{
        has: function(){
            if($localStorage.toast)
                return $localStorage.toast.show;
            else
                return false;
        },
        show: function(content) {
            var toast= $localStorage.toast;

            return $mdToast.show(
                $mdToast.simple()
                .content(toast.content)
                .position(toast.position)
                // .action(toast.action)
                .hideDelay(toast.delay)
                );
        },
        queue: function(setContent,setDelay,setPosition){
            var setDelay = typeof setDelay !== 'undefined' ? setDelay : 3000;
            var setPosition = typeof setPosition !== 'undefined' ? setPosition : 'bottom left';

            $localStorage.toast={show:true,content:setContent,delay:setDelay,position:setPosition};
        },
        clear: function(){
            delete $localStorage.toast;

        }
    }
}])
.factory('UpdateInfo', function($http, urls){
	return{
		change: function (data, success) {
            $http.post(urls.API + '/users/update/'+data.type, data).success(success);
        },
        userName: function(data, success){
        	$http.post(urls.API + '/users/'+data.type+'/check', data).success(success);
        },
        getTokenClaims: function () {
            return tokenClaims;
        },
        loggedIn: function(){return tokenClaims.sub ? true : false},
    }

})
.factory('User', function ($resource,urls) {

    return $resource(urls.API + '/users',{}, {
	'get': { method:'GET', cache: true, format: 'json'},
	'query': { method:'GET', cache: true, isArray:true, format: 'json' },
	'onlineUsers':{method:'GET', url: urls.API + '/users/onlineUsers', cache:true, format:'json'}
	});
})
.factory('userData', function($http,$q,urls,$timeout) {
    var cache;

    function getTheData() {

        var d = $q.defer();
        if( cache ) {
            d.resolve(cache);
        }
        else {

            $timeout(
                function(){

            $http.post(urls.API + '/access').then(
                function success(response) {
                    cache = response.data;
                    d.resolve(cache);
                },
                function failure(reason) {
                    d.reject(reason);
                }
            );

        },5000);
        }


        return d.promise;
    }

    function clearCache() {
        cache = null;
    }

    return {
        getData: function(){return getTheData()},
        clearCache: clearCache
    };
})
.factory('Villages', function ($resource,urls) {

    return $resource(urls.API + '/village',{}, {
		'get': { url: urls.API + '/village/:type',params: {type: '@type'}, method:'GET', cache: false, format: 'json'}	
	});
})
.run(function ($rootScope, $state, Auth,$localStorage) {
    $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
        
      if (toState.authenticate && !Auth.loggedIn()){
        // User isn’t authenticated  
        $state.transitionTo("guest.default",{reload:true});
        event.preventDefault();
      }
      // else 
      // {
      //   if($localStorage.verify !== '1')
      //   {
      //     $state.transitionTo("info.otp",{reload:true});
      //   }
      // }
      
    });
  })
.controller('footerCtrl', ['$scope', function($scope){

}])
.controller('homeContainerCtrl', ['$scope','$rootScope','$filter', 'Page','loginModal','signupModal', function($scope,$rootScope,$filter,Page,loginModal,signupModal){

	$rootScope.isGuest= true;
	
	Page.setTitle($filter('translate')('HOME_TITLE'));

	$scope.loginModalStatus= function(){return loginModal.get()};
	$scope.showLoginModal= function(){ loginModal.show(); signupModal.hide()};

	$scope.signupModalStatus= function(){return signupModal.get()};
	$scope.showSignupModal= function(){ signupModal.show(); loginModal.hide()};

}])
.controller('langCtrl', ['$scope','$state','locale', function($scope,$state,locale){

	$scope.languages=[{'title':'English','locale':'EN'},{'title':'हिंदी','locale':'HIN'},{'title':'ਪੰਜਾਬੀ','locale':'PUN'}];

	$scope.setLocale= function(){
		locale.setLocale($scope.currentLanguage);
		$state.reload();
	}

	$scope.currentLanguage= locale.getLocale();
}])
.controller('loginCtrl', ['$scope','loginModal','Auth','$localStorage','$mdToast','Toast','$state','$filter','urls', function($scope,loginModal,Auth,$localStorage,$mdToast,Toast,$state,$filter, urls){
	
	$scope.modalStatus= function(){return loginModal.get()};

	$scope.closeLoginModal= function(){ loginModal.hide()};

	function successAuth(res) {
	    $localStorage.token = res.token;
	    $localStorage.authId = res.id;
	    $localStorage.verify = res.verify;
	    
	   socket.emit('userStatus', {url:urls.API+"/user/status/online/"+res.id+"?token="+res.token, id:res.id});
	   if(res.verify == '1')
	   {
	   		Toast.queue($filter('translate')('LOGIN_MSG'));
	   		window.location = "/";
	   }
	   else
	   {
	   	window.location="/verify";
	   }
	   $state.reload();
	}

	$scope.login = function () {
		$scope.loginLoading=true;

	    var formData = {
	        mobile: $scope.user.mobile,
	        password: $scope.user.password
	    };

	    

	    Auth.login(formData, successAuth, function (resp) {
	    	$scope.loginLoading=false;
	        $mdToast.show(
	          $mdToast.simple()
	          .content(resp.message)
	          .position('top left')
	          .hideDelay(3000)
	        );
	    })
	};

}])
.controller('signupCtrl', ['$scope','signupModal','$mdToast', 'Assemblies', 'Villages', 'Parties', 'Toast', '$filter', 'UserSignup', '$localStorage', '$state','States', function($scope,signupModal,$mdToast, Assemblies, Villages, Parties, Toast, $filter, UserSignup, $localStorage, $state,States){
	// get states
  States.get({}, function(response){
    response.$promise.then(function(data){
      $scope.states = data.states;
    });
  });

  // get assemblies
  $scope.statesChange = function(){
    Assemblies.get({state:$scope.user.state},function(response){
      response.$promise.then(function(assembly){
        $scope.assemblies = assembly.data;
      });
    });
  }
  // get parties
  Parties.get({}, function(response){
    response.$promise.then(function(party){
      $scope.parties = party.data;
    });
  });
  

  $scope.modalStatus= function(){return signupModal.get()};

  $scope.closeSignupModal= function(){ signupModal.hide()};

  // assembly accordig  to get the village
  $scope.assemblyChange = function(assembly){
    $scope.assembly = assembly;
    Villages.get({type:$scope.assembly},function(response){
      response.$promise.then(function(villages){
          $scope.wards = villages.data;
         
      });
    });
  };

function successAuth(res) {
      $localStorage.token = res.token;
      $localStorage.token_id = res.token_id;
      $localStorage.authId = res.id;
      Toast.queue($filter('translate')('LOGIN_MSG'));
      window.location="/verify";
}
  $scope.user={};
  $scope.submit= function(){
    $scope.signupLoading = true;
    var data = $scope.user;
      UserSignup.create(data, function (response) {
        $scope.signupLoading = false;
        console.log(response);
        $scope.loginLoading=false;
        
          // $mdToast.show(
          //   $mdToast.simple()
          //   .content('Could not login!')
          //   .position('top left')
          //   .hideDelay(3000)
          // );
          if(response.error == false)
          {
            successAuth(response);
          }
        
          if(response.errorMsg == 'already')
          $scope.signupLoading=false;
          $scope.messageError = "Your Mobile number already registered";

      }, function(error){
        $scope.signupLoading=false;
         $scope.messageError = "Your Mobile number already registered";
      });

  };
   
 }])
.controller('buddyUpAllCtrl',['$scope','$rootScope','Page','User', 'urls','$q','BuddyUp','$localStorage','$state','$http', function($scope,$rootScope,Page,User,urls,$q,BuddyUp,$localStorage, $state,$http){
  Page.setTitle("Buddy Up");
  $scope.IMAGE_PATH = urls.IMAGE_PATH;
  $scope.authId = $localStorage.authId;
  $scope.users = '';
  $scope.userGet = function()
  {
    User.onlineUsers(function(response){
        response.$promise.then(function(users){
          $scope.users = users.users;
      });
    });
  };
  $scope.userGet();

  
  $scope.buddyUp = function(id, en){
    $scope.users[en].friend=1;
    $scope.users[en].view = 0;
    BuddyUp.send({id:id}, function(response){
    });
  };

  $scope.buddyleave = function(id, index)
  {
    $scope.users[index].friend=0;
    $http.get(urls.API+"/buddy/unbuddy/"+id).then(function(response){
      
    });
  };

}])

.controller('buddyUpCtrl',['$scope','$rootScope','Page','Posts','addPosts','$q', function($scope,$rootScope,Page,Posts,addPosts,$q){
  Page.setTitle("Buddy Up");
}])

.controller('closeGroupCtrl',['$scope','Page','$stateParams','$localStorage','$state','urls','singleGroup', '$mdDialog', 'User','$http','Upload','Group', function($scope,Page, $stateParams, $localStorage, $state, urls, singleGroup, $mdDialog, User,$http,Upload,Group){
  Page.setTitle("Message");
	$scope.authId = $localStorage.authId;
	$scope.slug = $stateParams.group;
	$scope.IMAGE_PATH = urls.IMAGE_PATH;

	singleGroup.check({user:$scope.authId}, function(response){
		response.$promise.then(function(user){

			$scope.loginUserType = user.users;
			if(user.users == 'notMember')
			{
				$state.go('user.messages.groups');
			}
		});
	});

	singleGroup.get({slug:$scope.slug},function(response){
    	response.$promise.then(function(group){
    		
          $scope.groupDetail = group.group;
           $scope.members = group.members;
      	});
  	});

  	$scope.addMember = function(){
	  	$mdDialog.show({
	    	templateUrl: 'pages/user/message/addMember.view.html',
	    	parent: angular.element(document.body),
	    	clickOutsideToClose:false,      
	    });
  	};

  	$scope.closeCreateGroup = function() {
      $mdDialog.cancel();
      $state.reload();
	};
	$scope.Loading=false;

	$scope.add = function(groupID, userID){
		$scope.Loading= true;
		
		singleGroup.Add({type:'add',group:groupID, user:userID}, function(response){
			$scope.Loading=false;
			$scope.getUsers();

		});
	};

	$scope.remove = function(groupID, userID){
		$scope.Loading=true;
		singleGroup.Add({type:'remove', group:groupID, user:userID}, function(response){
			$scope.Loading= false;
			$scope.getUsers();
		});
	};

	$scope.getUsers = function(){
	  	singleGroup.allUser({slug:$scope.slug}, function(response){
	  		response.$promise.then(function(users){
	  			return $scope.users = users.users;
	  		});
	  	});
  	};
  	$scope.getUsers();

  	 $scope.showAlert = function(ev, id) {
	   var confirm = $mdDialog.confirm()
	   .title('are you sure you want to delete?')
	   .targetEvent(ev)
	   .ok('Okay')
	   .cancel('Cancel');
	   $mdDialog.show(confirm).then(function() {
	    
	    singleGroup.delete({id:id}, function(success){
	       $state.go('user.messages.groups');
	    });
	  }, function() {
	    $scope.status = 'You decided to keep your debt.';
		});
 	};

 	$scope.leftGroup = function(id, groupID)
 	{
 		singleGroup.left({id:id, groupID:groupID}, function(success){
 			$state.go('user.messages.groups');
 		});
 	};

 	$scope.groupEdit = function()
 	{
 		$mdDialog.show({
      		templateUrl: 'pages/user/partials/groupEdit.view.html',
      		parent: angular.element(document.body),
    		clickOutsideToClose:false,      
    	});
 	};

 	$scope.closeCreateGroup = function() {
      $mdDialog.cancel();
	};

	$scope.uploadedImages = '';
	$scope.updateGroupImage = function(files){
		
   if (files && files.length) {
        for (var k = 0; k < files.length; k++) {

          if(typeof $scope.images == 'undefined')
            $scope.images=[];

          $scope.images.push(files[k]);

          files[k].id= $scope.images.length-1;
     
        }
        
        for (var i = 0; i < files.length; i++) {

          Upload.upload({
            url: urls.API + '/posts/group-image',
            data: {image: files[i]}
        }).then(function (resp) {
          
          $scope.uploadedImages=resp.data.image;

          if($scope.uploadedImages != null)
          {
            $scope.shareButton=true;
          }
          
        }, function (resp) {
            
        }, function (evt) {
          var progressPercentage= parseInt(100.0 * evt.loaded / evt.total);

          $scope.images[evt.config.data.image.id]['progress']= progressPercentage;
            
        });

     
        }
      }
	};

	$scope.updateGroup = function()
	{
		if($scope.uploadedImages != '')
		{
			$scope.groupDetail.image = $scope.uploadedImages;
		}
		
		Group.update($scope.groupDetail, function(resp){
		
			$state.go('user.messages.closeGroup', {group:resp.newSlug});
			$mdDialog.cancel();
		});
	};


}])
.controller('groupListCtrl',['$scope','Page','$sce','$mdSidenav','$filter','$http','urls','$localStorage', function($scope,Page,$sce, $mdSidenav, $filter,$http, urls,$localStorage){
	  Page.setTitle($filter('translate')('Groups'));
	  $scope.authID = $localStorage.authId;
	  
	   $scope.rightToggle = function(menuId) {
	    $mdSidenav(menuId).toggle();
	  };
	  $scope.IMAGE_PATH = urls.IMAGE_PATH;
	  $scope.groups = '';
	  $http.get(urls.API+"/group/get/all").then(function(success){
	  	$scope.groups = success.data.result;
	  });

	  $scope.join = function(ind, groupid)
	  {
	  	$scope.groups[ind].joined = 1;
	  	
	  };
	  
}])
.controller('MessageCtrl',['$scope','Page','$sce', 'urls', 'chatSocket', 'User','$localStorage', '$timeout','getChat', function($scope,Page,$sce, urls, chatSocket, User, $localStorage, $timeout, getChat){
  Page.setTitle("Message");
  $scope.authId = $localStorage.authId;
  $scope.typing=false;
  $scope.IMAGE_PATH= urls.IMAGE_PATH;
   $scope.isOpen = false;
      $scope.demo = {
        isOpen: false,
        count: 0,
        selectedDirection: 'right'
      };
      var messages = {};
    User.onlineUsers(function(response){
        response.$promise.then(function(users){
          $scope.users = users.users;
          
      });
    });
   
    $scope.users = [];
    $scope.messages = [];
    chatSocket.connect();

    $scope.messageSendSelect = function(user){
    	$scope.name = user.name;
      $scope.avatar = user.avatar;
      $scope.id = user.id;
      chatSocket.emit('add-user', {userid:user.id});
      getChat.get({sender:$scope.authId, receive:$scope.id},function(response){
       
        $scope.messages =[];
        response.$promise.then(function(chat){
          
          var data = chat.chat;

          for(i=0;i<=data.length;i++)
          { 
            $scope.messages.push(data[i]);
          }
          
          
        });
      });
      
    };
    

    $scope.typingStart= function(){
      if($scope.message != '')
      {
        chatSocket.emit('status', {status:'1', received:$scope.id});
      }
      else
      {
        chatSocket.emit('status', {status:'0', received:$scope.id});
      }
    };

    chatSocket.on('status', function(data){
      $scope.status = data;
      if(data.status == 1){
        $scope.typing=true;  
      }
      else{
        $scope.typing=false;
      }
    });

    $scope.sendMessage = function(){
      if($scope.message != null && $scope.message != '')
      {
        // socket.emit('chat message', $('#m').val());
        socket.emit('message', {message:$scope.message, sender:$scope.authId, receiver:$scope.id});
        $scope.message = '';
      }
    };

    socket.on('users', function(data){
        $scope.users = data.users;
    });

    socket.on('message', function(data){
      $scope.messages.push(data);
      console.log(data);
    });

    console.log($scope.messages , 'messages');

}])
.controller('MessageMainCtrl',['$scope','Page','$sce','$mdSidenav', function($scope,Page,$sce, $mdSidenav){
  Page.setTitle("Message");
   $scope.rightToggle = function(menuId) {
    $mdSidenav(menuId).toggle();
  };

}])
.controller('newsCtrl',['$scope','Page', function($scope,Page){

  Page.setTitle("News");
  $scope.name="my data 345435";
}])

.controller('answeredCtrl', ['$scope','Page','$stateParams','$http','urls','$mdDialog','$mdToast','$timeout', function($scope, Page, $stateParams,$http,urls,$mdDialog,$mdToast,$timeout){
	Page.setTitle('Answered');

	$http.get(urls.API+"/question/result/"+$stateParams.id).then(function(success){
		$scope.question = success.data.question;
		$scope.results = success.data.result;
		for (var k = 0; k < success.data.result.length; k++) {
		      		success.data.result[k].percent=0;
		      	};
		      	$scope.results=success.data.result;

		      	$timeout(function() {
		        for (var i = 0; i < $scope.results.length; i++) {
		        	$scope.results[i].percent= $scope.results[i].per;
		        };
		}, 1500);
	});
}])
.controller('issueCtrl',['$scope','Page','$sce', 'Parties', 'nextCmAssembly','$http', 'urls','$mdToast', function($scope,Page,$sce, Parties,nextCmAssembly,$http,urls,$mdToast){
  Page.setTitle("Public Issue");
  $scope.name="my data";
  
  nextCmAssembly.get({},function(response){
    response.$promise.then(function(party){
      $scope.parties = party.party;
    });
    response.$promise.then(function(assembly){
      $scope.assemblies = assembly.assembly;
    });
  });

  $scope.assemblyChange = function(assembly){
    $scope.assembly = assembly;
    nextCmAssembly.village({type:$scope.assembly},function(response){
      response.$promise.then(function(villages){
          $scope.villages = villages.data;
      });
    });
  };
    
  $scope.issueSubmit = function()
  {
    $http.post(urls.NEXTCM_API + '/issue', $scope.user).then(function(response){
       $mdToast.show(
            $mdToast.simple()
                .content(response.data.data)
                .position('bottom left')
               .hideDelay(3000)
            ); 
       $scope.user = '';
    }, function(error){
    });
  };

}])
.controller('nextcmCtrl',['$scope','Page','$sce', 'nextCmAssembly', 'Villages', '$http', 'urls', '$mdToast', '$mdDialog','$localStorage','$timeout', function($scope,Page,$sce, nextCmAssembly, Villages, $http, urls, $mdToast, $mdDialog,$localStorage,$timeout){
  Page.setTitle("NextCM");
  // $scope.name="my data";
  
    nextCmAssembly.get({},function(response){
      response.$promise.then(function(party){
        $scope.parties = party.party;
      });
      response.$promise.then(function(assembly){
        $scope.assemblies = assembly.assembly;
      });
    });

    $scope.assemblyChange = function(assembly){
      $scope.assembly = assembly;
      nextCmAssembly.village({type:$scope.assembly},function(response){
        response.$promise.then(function(villages){
            $scope.wards = villages.data;
        });
      });
    };
    
  $scope.cmGet = function(){
       nextCmAssembly.cm({type:$scope.nextcm.party},function(response){
        response.$promise.then(function(cm){
          $scope.cms = cm.data;
      });
    });
  };

 

  $scope.tokenID = "";
  // timer set   
        $scope.counter = 59;
        $scope.onTimeout = function(){
          if($scope.counter != 0)
          {
            $scope.counter--;
            mytimeout = $timeout($scope.onTimeout,1000);
          }
        };
        var mytimeout = $timeout($scope.onTimeout,1000);

  $scope.submit = function(ev){
    $scope.loginLoading=true;
     $http.post(urls.NEXTCM_API + '/nextcm', $scope.nextcm).then(
                function success(response) {
                  $scope.nextcm = '';
                  $scope.onTimeout();
                    $mdDialog.show({
                      templateUrl: 'pages/user/partials/OTP.view.html',
                      parent: angular.element(document.body),
                      clickOutsideToClose:true,      
                    });

                    $localStorage.tokenID = response.data.data.id;
                      
                    $scope.loginLoading=false;
                },
                function failure(reason) {
                 if(reason.data.errorMsg == 'MOBILE_USED')
                 {
                   $mdToast.show(
                      $mdToast.simple()
                      .content('This Mobile number already used')
                      .position('bottom left')
                      .hideDelay(3000)
                    ); 
                 }
                 if(reason.data.errorMsg.mobile !== undefined && reason.data.errorMsg.mobile !== null)
                 {
                   $mdToast.show(
                      $mdToast.simple()
                      .content(reason.data.errorMsg.mobile[0])
                      .position('bottom left')
                      .hideDelay(3000)
                    ); 
                 }
                 if(reason.data.errorMsg.name !== undefined && reason.data.errorMsg.name !== null)
                 {
                   $mdToast.show(
                      $mdToast.simple()
                      .content(reason.data.errorMsg.name[0])
                      .position('bottom left')
                      .hideDelay(3000)
                    ); 
                 }
                  $scope.loginLoading=false;

                }
            );
  };

    $scope.OTPVerify = function()
    {
      var data ={token:$scope.OTP, id:$localStorage.tokenID};
      $http.post(urls.NEXTCM_API+'/nextcm/verify', data).then(function(response){
            if(response.data.data != 0)
            {
              $mdDialog.cancel();
                $mdToast.show(
                    $mdToast.simple()
                    .content('Your vote counted.')
                    .position('bottom left')
                    .hideDelay(3000)
                  ); 
            }
            if(response.data.data == 0)
            {
                $mdToast.show(
                    $mdToast.simple()
                    .content('Invalid OTP')
                    .position('bottom left')
                    .hideDelay(5000)
                ); 
            }
          });

    };

    $scope.closeModel = function()
    {
      $mdDialog.cancel();
    };

    $scope.resend = function()
    {
      $localStorage.tokenID;
      $http.get(urls.NEXTCM_API+'/resend/otp/'+$localStorage.tokenID);
            $scope.counter = 59;
            $scope.onTimeout();
    };

}])
.controller('nextmlaCtrl',['$scope','Page','$sce', 'Parties', 'Assemblies', 'Villages','nextCmAssembly','$http','urls','$timeout','$mdToast','$mdDialog','$localStorage', function($scope,Page,$sce, Parties, Assemblies, Villages,nextCmAssembly,$http,urls,$timeout,$mdToast,$mdDialog,$localStorage){
  Page.setTitle("NextMLA");
  $scope.name="my data";
  
   nextCmAssembly.get({},function(response){
    response.$promise.then(function(party){
      $scope.parties = party.party;
    });
    response.$promise.then(function(assembly){
      $scope.assemblies = assembly.assembly;
    });
  });

  $scope.assemblyChange = function(assembly){
    $scope.assembly = assembly;
    nextCmAssembly.village({type:$scope.assembly},function(response){
      response.$promise.then(function(villages){
          $scope.wards = villages.data;

      });
    });
  };
 
  $scope.MLAGet = function()
  {

    $http.get(urls.NEXTCM_API+"/"+$scope.opinion.assembly+"/"+$scope.opinion.party+"/mla").then(function(response){
      $scope.mlas= response.data.data;

    }, function(error){

    })
  };

  $scope.tokenID = "";
  // timer set   
        $scope.counter = 59;
        $scope.onTimeout = function(){
          if($scope.counter != 0)
          {
            $scope.counter--;
            mytimeout = $timeout($scope.onTimeout,1000);
          }
        };
        var mytimeout = $timeout($scope.onTimeout,1000);

  $scope.submit = function()
  { 
     $http.post(urls.NEXTCM_API + '/nextmla', $scope.opinion).then(function(response) {

        $localStorage.nextmlaID = response.data.data.id;
        $scope.onTimeout();
          $mdDialog.show({
              templateUrl: 'pages/user/partials/mlaOTP.view.html',
              parent: angular.element(document.body),
              clickOutsideToClose:true,      
          });
          $scope.opinion = '';
     }, function(reason){
        if(reason.data.errorMsg == 'MOBILE_USED')
                 {
                   $mdToast.show(
                      $mdToast.simple()
                      .content('This Mobile number already used')
                      .position('bottom left')
                      .hideDelay(3000)
                    ); 
                 }
                 if(reason.data.errorMsg.mobile !== undefined && reason.data.errorMsg.mobile !== null)
                 {
                   $mdToast.show(
                      $mdToast.simple()
                      .content(reason.data.errorMsg.mobile[0])
                      .position('bottom left')
                      .hideDelay(3000)
                    ); 
                 }
                 if(reason.data.errorMsg.name !== undefined && reason.data.errorMsg.name !== null)
                 {
                   $mdToast.show(
                      $mdToast.simple()
                      .content(reason.data.errorMsg.name[0])
                      .position('bottom left')
                      .hideDelay(3000)
                    ); 
                 }
                  $scope.loginLoading=false;
     });
  };

    $scope.OTPVerify = function()
    {
      var data ={token:$scope.OTP, id:$localStorage.nextmlaID};
      $http.post(urls.NEXTCM_API+'/nextmla/verify', data).then(function(response){
            if(response.data.data != 0)
            {
              $mdDialog.cancel();
                $mdToast.show(
                    $mdToast.simple()
                    .content('Your vote counted.')
                    .position('bottom left')
                    .hideDelay(3000)
                  );
                  $localStorage.nextmlaID = '';
            }
            if(response.data.data == 0)
            {
                $mdToast.show(
                    $mdToast.simple()
                    .content('Invalid OTP')
                    .position('bottom left')
                    .hideDelay(5000)
                ); 
            }
          });

    };

    $scope.closeModel = function()
    {
      $mdDialog.cancel();
    };

    $scope.resend = function()
    {
      $localStorage.tokenID;
      $http.get(urls.NEXTCM_API+'/resend/otp/'+$localStorage.nextmlaID);
            $scope.counter = 59;
            $scope.onTimeout();
    };
  

}])
.controller('opinionCtrl',['$scope','Page','$sce','$mdSidenav', function($scope,Page,$sce,$mdSidenav){
  Page.setTitle("Public Opinion");
  $scope.name="my data";
 $scope.rightToggle = function(menuId) {
    $mdSidenav(menuId).toggle();
  };
}])
.controller('publicOpinionCtrl',['$scope','Page','$sce','$http','urls', function($scope,Page,$sce,$http,urls){
  Page.setTitle("Public Opinion");
  $scope.name="my data";
  $http.get(urls.API+"/public/opinion").then(function(response){
  	$scope.questions = response.data.question;
  });
}])
.controller('questionCtrl', ['$scope','Page','$stateParams','$http','urls','$mdDialog','$mdToast', function($scope, Page, $stateParams,$http,urls,$mdDialog,$mdToast){
	Page.setTitle('Unanswared');
	
	$http.get(urls.API+'/question/get/'+$stateParams.id).then(function(success){
		$scope.question = success.data.question;
	});

	$scope.submitOpinion = function(option, question){
		$scope.question.totalOpinion++;
		$scope.question.i_opinion = '1';
		
		$http.get(urls.API+'/opinion/submit/'+option+'/'+question).then(function(response){
		
			 $mdDialog.cancel();
                $mdToast.show(
                    $mdToast.simple()
                    .content('Your vote counted.')
                    .position('bottom left')
                    .hideDelay(3000)
                );
		});
	};
}])

.controller('commentsCtrl', ['$mdDialog', function($mdDialog){
  
}])
.controller('fabDialCtrl',['$scope','$mdDialog', function($scope,$mdDialog){

  $scope.fabOptions=[
  {'title' : 'Add Audio','name': 'audio','icon' : 'audiotrack'},
  {'title' : 'Add Video','name': 'video','icon' : 'videocam'},
  {'title' : 'Add Images','name': 'images', 'icon' : 'image'},
  {'title' : 'Add Status','name': 'status', 'icon' : 'send'}
  ];



  function addStatusCtrl($scope,option){
    $scope.option=option;

    $scope.closeModal = function() {
      $mdDialog.cancel();
    };
  }

  $scope.submit= function(ev,option,user){


    $mdDialog.show({
      controller: addStatusCtrl,
      templateUrl: 'pages/user/partials/status-modal.view.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      escapeToClose : false,
      locals: {option:option}
    });
  };

}])
.controller('footerCtrl', ['$scope', function($scope){

}])
.controller('imageModelCtrl', ['$mdDialog', function($mdDialog){
  
}])

.controller('leftSideCtrl',['$scope','$rootScope','Page','Posts','addPosts','$q','$mdDialog','$http','urls','$state', function($scope,$rootScope,Page,Posts,addPosts,$q,$mdDialog,$http, urls,$state){
  
 $scope.joinGroup = function()
 {
 	$state.go('user.messages.groups');
 };

  $scope.IMAGE_PATH = urls.IMAGE_PATH;

  	$http.get(urls.API+"/group/get/users").then(function(success){
  		$scope.groups = success.data.result;
  	});

  	$scope.openGroup = function(slug)
  	{
  		$state.go('user.messages.closeGroup', {"group":slug});
  	}
}])
.controller('navbarCtrl',['$scope','$mdSidenav','Auth', '$mdDialog', '$state', 'Notification', '$timeout', '$interval', 'NotificationGet', 'urls', 'Upload', 'Group', '$mdToast', 'BuddyRequest','Parties','User','$localStorage', function($scope,$mdSidenav,Auth, $mdDialog, $state, Notification, $timeout, $interval, NotificationGet, urls, Upload, Group, $mdToast, BuddyRequest,Parties,User,$localStorage){
  $scope.toggleSidenav = function(menuId) {
    $mdSidenav(menuId).toggle();
  };


  $scope.IMAGE_PATH = urls.IMAGE_PATH;
  
  $scope.notiCount=0;

  $scope.goHome = function()
  {
    // window.location="#/home";
  	 $state.go('user.timeline.public');
  };

  $scope.seeAllBuddy = function()
  {
    $state.go('user.buddy.allBuddy');
  };

  $scope.messages = function()
  {
    // window.location="#/messages";
    $state.go('user.messages.msg');
  };

   $scope.logout= function(){
    socket.emit('userStatus', {url:urls.API+"/user/status/offline/"+$localStorage.authId+"?token="+$localStorage.token});
    $scope.logoutLoading=true;
    $localStorage.verify = '';
    Auth.logout();
  }

  $scope.requestFound = false;

  this.loadNotifications = function (){
      Notification.notification().then(function(data){
        $scope.notiCount = data.data.count;
        $scope.request = data.data.request;
        if($scope.request != '0')
          {
            $scope.requestFound = true;
          }
      });
   };

   //Put in interval, first trigger after 10 seconds 
   $interval(function(){
      this.loadNotifications();
   }.bind(this), 1000);    

   //invoke initialy
   this.loadNotifications();

$scope.notifiGet = function($mdOpenMenu, ev){
    $mdOpenMenu(ev);
    NotificationGet.getNotification().then(function(data){
      $scope.notifications = data.data.notification;
    });
    NotificationGet.View().then(function(data){
    });
}; 
$scope.loading = true;  
$scope.requestsGet = '';
$scope.buddyRequest = function($mdOpenMenu, ev){
    $mdOpenMenu(ev);
    BuddyRequest.getRequest().then(function(response){
      $scope.requestsGet =response.data.request;
      $scope.loading = false;  
    }, function(response){
      $scope.loading = false;  
    });
    BuddyRequest.view();
};

$scope.acceptRequest = function(buddyID, ev)
{
  $scope.requestsGet[ev].view = '1';
  BuddyRequest.accept(buddyID).then(function(response){
  });
}

$scope.closeCreateGroup = function() {
      $mdDialog.cancel();
};

$scope.createGroup = function(menu){
  $mdSidenav(menu)
   .close()
   .then(function(){
     // $log.debug('closed');
   });
   $mdDialog.show({
      templateUrl: 'pages/user/partials/createGroup.view.html',
      parent: angular.element(document.body),
      // targetEvent: ev,

      clickOutsideToClose:false,      
      // locals: {currentPost:currentPost,IMAGE_PATH:$scope.IMAGE_PATH}
    });
};
 $scope.uploadedImages='';
 $scope.shareButton = false;

$scope.updateGroupImage = function(files){
   if (files && files.length) {
        for (var k = 0; k < files.length; k++) {

          if(typeof $scope.images == 'undefined')
            $scope.images=[];

          $scope.images.push(files[k]);

          files[k].id= $scope.images.length-1;
     
        }
        for (var i = 0; i < files.length; i++) {

          Upload.upload({
            url: urls.API + '/posts/group-image',
            data: {image: files[i]}
        }).then(function (resp) {
          $scope.uploadedImages=resp.data.image;

          if($scope.uploadedImages != null)
          {
            $scope.shareButton=true;
          }
          
        }, function (resp) {
        }, function (evt) {
          var progressPercentage= parseInt(100.0 * evt.loaded / evt.total);

          $scope.images[evt.config.data.image.id]['progress']= progressPercentage;
        });

     
        }
      }
};


$scope.submitGroup = function(){
  
  $scope.data = {name:$scope.group.name, image:$scope.uploadedImages};
    Group.create($scope.data, function(resp){
      $mdDialog.hide();
      $state.go('user.messages.closeGroup', {group:resp.slug});
    }, function(error){
      if(error.message == 'notvalid')
      {
          $mdToast.show(
            $mdToast.simple()
            .content("This name already Register")
            .position('top left')
            .hideDelay(5000)
          );
      }
    });
  };

  $scope.allParty = function(menu)
  {
    $mdSidenav(menu)
      .close()
   .then(function(){
     // $log.debug('closed');
   });
    $mdDialog.show({
      templateUrl: 'pages/user/partials/allParties.view.html',
      parent: angular.element(document.body),
      // targetEvent: ev,

      clickOutsideToClose:false,      
      // locals: {currentPost:currentPost,IMAGE_PATH:$scope.IMAGE_PATH}
    });
  };

   Parties.get({}, function(response){
    response.$promise.then(function(party){
      $scope.parties = party.data;
    });
  });

  User.get({},function(user){
    $scope.user= user.data;  
  });

  $scope.openPage = function(slug)
  {
    $mdDialog.cancel();
    $state.go('parties.page', {party:slug});
  }  

}])
.controller('OnlineUserCtrl',['$scope','Page','$sce', 'urls','$http', function($scope,Page,$sce, urls,$http){
	 var imagePath = 'http://api.rajneeti.dev/images/profile/default_avatar.png';
  $scope.IMAGE_PATH = urls.IMAGE_PATH;
    $http.get(urls.API+'/users/friends/get').then(function(resp){
      
      $scope.friends = resp.data.friends;
    });	  
}])
.controller('rightSide',['$scope','$rootScope','$q','$http','urls','$localStorage','$state','User', function($scope,$rootScope,$q,$http,urls,$localStorage,$state,User){
  
  $scope.IMAGE_PATH= urls.IMAGE_PATH;
  $scope.authId = $localStorage.authId;
  User.get({},function(user){
    $scope.user= user.data;  
  });
  
  $scope.messages =[];
  $http.get(urls.API+"/user/party").then(function(response){
  	$scope.party = response.data.likes;
  });

  $scope.friends="";
  var imagePath = 'http://api.rajneeti.dev/images/profile/default_avatar.png';
  $scope.IMAGE_PATH = urls.IMAGE_PATH;
    $http.get(urls.API+'/users/friends/get').then(function(resp){
      $scope.friends = resp.data.friends;
      $scope.totalOnline = resp.data.totalOnline;
    });
    socket.emit('friends', {token:$localStorage.token});
    socket.on('friends', function(data){
      $scope.friends = data.friends;
      $scope.totalOnline = data.totalOnline;
    });

    
  $scope.chatPopups = $localStorage.popup;
 
  $scope.close = function(index)
  {
  	 $localStorage.popup.splice(index, 1); 
  };

  if($localStorage.popup === undefined)
  {
    $scope.items=[];
  }
  else
  {
    $scope.items = $localStorage.popup;
  }
  $scope.openChat = function(index)
  {
    socket.emit('getChat', {sender:$scope.authId, receiver:$scope.friends[index]['id']});
    $scope.items.push($scope.friends[index]);
    $localStorage.popup = $scope.items;
    $state.reload();
  };

  if($localStorage.popup !== undefined)
  {
    for(var b = 0;b<$localStorage.popup.length;b++)
    {
      socket.emit('getChat', {sender:$scope.authId, receiver:$localStorage.popup[b]['id']});
    }
  }

  socket.on('getChat', function(data){
    for(var i=0;i<data.length;i++)
    {
      $scope.messages.push(data[i]);
    }
  });

  

  
  $scope.sendMessage = function(id, message)
  {
    socket.emit('message', {message:message, sender:$scope.authId, receiver:id});
  }
  // message send and output recieve 
    socket.on('message', function(data){
      $scope.messages.push(data);
      console.log(data);
    });

}])
.controller('tabsCtrl', function($scope, $state, $log) {

    $scope.$watch('selectedTab', function(current, old) {
        switch (current) {
            case 0:
                $state.transitionTo('user.timeline.public');
                break;
            case 1:
                $state.transitionTo('user.timeline.party');
                break;
            case 2:
                $state.transitionTo('user.timeline.assembly');
                break;
        }
    });
})
.controller('partiesMainCtrl', ['$scope','Page','urls','Auth','$mdToast','$localStorage','Toast','$filter', function($scope, Page,urls,Auth,$mdToast,$localStorage,Toast,$filter){
	Page.setTitle('Parties');
	$scope.IMAGE_PATH = urls.IMAGE_PATH;
	$scope.loginCheck = Auth.loggedIn();

	function successAuth(res) {
	    $localStorage.token = res.token;
	    $localStorage.authId = res.id;
	    Toast.queue($filter('translate')('LOGIN_MSG'));
	   // $state.go('user.timeline.public');
	   window.location="/";
	   $state.reload();
	}


	$scope.login = function (mobile, password) {
		 
		$scope.loginLoading=true;

	    var formData = {
	        mobile: mobile,
	        password: password
	    };


	    Auth.login(formData, successAuth, function (resp) {
	    	$scope.loginLoading=false;
	        $mdToast.show(
	          $mdToast.simple()
	          .content(resp.message)
	          .position('top left')
	          .hideDelay(3000)
	        );
	    })
	};
}])
.controller('partyPageCtrl', ['$scope','Page','$http','urls','$stateParams','Auth', function($scope,Page, $http, urls,$stateParams,Auth){
Page.setTitle('party Page');	
	$http.get(urls.API+"/party/page/"+$stateParams.party).then(function(response){
		$scope.detail = response.data.result;
		Page.setTitle($scope.detail.eng_name);
		$scope.national= response.data.national_president;
		$scope.state_parbhari= response.data.state_parbhari;
		$scope.state_president= response.data.state_president;
		$scope.candidates = response.data.candidates;
	});
	
	$scope.detail = {i_like:0};

	$scope.like = function(detail, type){
		if(Auth.loggedIn())
		{
			console.log('login is');
			if(type == 'like')
			{
				detail.i_like = 1;
				detail.likes++;
			}
			else
			{
				detail.i_like = 0;
				detail.likes--;
			}
			$http.get(urls.API+"/page/"+detail.id+"/"+type);
		}
		else
		{
			$scope.withoutLogin = true;
		}
	}
}])
  .controller('coverCtrl', ['$scope','Posts','Upload','urls', '$localStorage', '$state', 'UpdateInfo', '$mdDialog', function($scope,Posts,Upload,urls, $localStorage, $state, UpdateInfo, $mdDialog){
  $scope.uploadedImages='';
  $scope.shareButton=false;
  $scope.uploadFiles = function (files) {
      if (files && files.length) {
        for (var k = 0; k < files.length; k++) {

          if(typeof $scope.images == 'undefined')
            $scope.images=[];

          $scope.images.push(files[k]);

          files[k].id= $scope.images.length-1;
     
        }

        for (var i = 0; i < files.length; i++) {

          Upload.upload({
            url: urls.API + '/posts/cover',
            data: {image: files[i]}
        }).then(function (resp) {
          $scope.uploadedImages=resp.data.image;
          if($scope.uploadedImages != null)
          {
            $scope.shareButton=true;
          }
          
        }, function (resp) {
        }, function (evt) {
          var progressPercentage= parseInt(100.0 * evt.loaded / evt.total);

          $scope.images[evt.config.data.image.id]['progress']= progressPercentage;
        });

     
        }
      }
    };
    $scope.closeModal = function() {
      $mdDialog.cancel();
    };

    $scope.updateCover= function(){
      UpdateInfo.change({value: $scope.uploadedImages, type: 'cover'}, function(response){
        $state.reload();
         $mdDialog.cancel();
        }, function(response){
        });
    };
  }])
.controller('profileCtrl',['$scope','Page', '$stateParams', 'Posts', '$q', 'addPosts', '$localStorage', '$mdDialog', '$mdMedia', '$state', function($scope,Page, $stateParams, Posts, $q, addPosts, $localStorage, $mdDialog, $mdMedia, $state){
  Page.setTitle($stateParams.slug);
  $scope.name=$stateParams.slug;

  function commentsCtrl($scope,$mdDialog,Posts,currentPost,IMAGE_PATH){

    $scope.post=currentPost;
    $scope.IMAGE_PATH=IMAGE_PATH;


    $scope.updated=0;

    $scope.isLoading= true;

    $scope.comments= new Array();
      
  
    Posts.comments({id:$scope.post.id},function(response){
      $scope.comments= response.data;

      if(response.data.length == 0)
        $scope.noComments=true;

      $scope.post.comment_count=response.data.length;

      $scope.isLoading= false;



    });



    $scope.closeComments = function() {

      $mdDialog.cancel();
    };

    $scope.addComment= function(){

      if( (typeof $scope.newComment !== 'undefined' ? $scope.newComment.length : 0 ) > 2){
        Posts.addComment({id:$scope.post.id,comment: $scope.newComment}, function(response){
          $scope.comments.unshift(response.data);

          $scope.updated++;
          $scope.noComments=false;

          $scope.post.comment_count++;

        });

        $scope.newComment="";
      }
    }

  }
  $scope.showCommentsProfile = function(currentPost) {
    $mdDialog.show({
      controller: commentsCtrl,
      templateUrl: 'pages/user/partials/comments.view.html',
      parent: angular.element(document.body),
      // targetEvent: ev,
      clickOutsideToClose:true,      
      locals: {currentPost:currentPost,IMAGE_PATH:$scope.IMAGE_PATH}

    });
  };

  $scope.profileCover = function(userId) {
    $mdDialog.show({
      templateUrl: 'pages/user/profile/coverModel.view.html',
      parent: angular.element(document.body),
      // targetEvent: ev,
      clickOutsideToClose:true,      
      locals: {currentPost:userId,IMAGE_PATH:$scope.IMAGE_PATH}

    });
  };

    $scope.profilePhoto = function(userId) {
    $mdDialog.show({
      templateUrl: 'pages/user/profile/profilePic.view.html',
      parent: angular.element(document.body),
      // targetEvent: ev,
      clickOutsideToClose:true,      
      locals: {currentPost:userId,IMAGE_PATH:$scope.IMAGE_PATH}

    });
  };

  



  $scope.profilePostLike= function(post,id,type){
    //like
    if(type=='like'){
      //Doesn't like already, gotta like
      if(post.i_like != 1){
        post.like_count++;
        post.i_like=1;
        Posts.like({id:id,type:type});
      }

      post.like_type=1;

      return post;
    }

    //dislike

    //Liked earlier, gotta dislike
    if(post.i_like == 1){
      post.i_like=0;
      post.like_count--;
    }

    Posts.like({id:id,type:type});

    post.like_type=0;

    return post;
  };


  $scope.showAlert = function(ev, id) {
   var confirm = $mdDialog.confirm()
   .title('are you sure you want to delete?')
   .targetEvent(ev)
   .ok('Okay')
   .cancel('Cancel');
   $mdDialog.show(confirm).then(function() {
    Posts.delete({id:id}, function(response){
       $state.reload();
    });
  }, function() {
    $scope.status = 'You decided to keep your debt.';
  });
 };


  $scope.authId = $localStorage.authId;
  
  $scope.postsLoading=true;

  $scope.postType="public";

  Posts.profile({slug:$scope.name, 'offset': 0},function(response){
    $scope.posts= response.data;
    $scope.usersDetail = response.user;
    $scope.postsLoading=false;
  });

  $scope.likePost= function(index,id,type){
    
    $scope.posts[index]= $scope.profilePostLike($scope.posts[index], id, type);
  };

   //Load more posts when required
  $scope.loadMorePosts= function(){

    var deferred = $q.defer();


    addPosts.add($scope.postType,typeof $scope.posts == 'undefined' ? [] : $scope.posts).then(function(response){
      $scope.posts= response;
      deferred.resolve(true);
    });

    return deferred.promise;
    
  }

$scope.sharePost = function(postID, userID)
   {
    Posts.share({postId:postID, userId:userID}, function(response){
      $state.reload();
    });
   };

}])
  .controller('profilePicCtrl', ['$scope','Posts','Upload','urls', '$localStorage', '$state', 'UpdateInfo', '$mdDialog', function($scope,Posts,Upload,urls, $localStorage, $state, UpdateInfo, $mdDialog){
  $scope.uploadedImages='';
  $scope.shareButton=false;
  $scope.uploadFiles = function (files) {
      if (files && files.length) {
        for (var k = 0; k < files.length; k++) {

          if(typeof $scope.images == 'undefined')
            $scope.images=[];

          $scope.images.push(files[k]);

          files[k].id= $scope.images.length-1;
     
        }

        for (var i = 0; i < files.length; i++) {

          Upload.upload({
            url: urls.API + '/posts/photo',
            data: {image: files[i]}
        }).then(function (resp) {
          $scope.uploadedImages=resp.data.image;
          if($scope.uploadedImages != null)
          {
            $scope.shareButton=true;
          }
          
        }, function (resp) {
        }, function (evt) {
          var progressPercentage= parseInt(100.0 * evt.loaded / evt.total);

          $scope.images[evt.config.data.image.id]['progress']= progressPercentage;
        });

     
        }
      }
    };
    $scope.closeModal = function() {
      $mdDialog.cancel();
    };

    $scope.updateProfilePic= function(){
      UpdateInfo.change({value: $scope.uploadedImages, type: 'profilePic'}, function(response){
        $state.reload();
         $mdDialog.cancel();
        }, function(response){
        });
    };
  }])
.controller('generalCtrl',['$scope','Page','$sce', 'User', 'UpdateInfo', '$state','$mdDialog', function($scope,Page,$sce, User, UpdateInfo, $state,$mdDialog){
  Page.setTitle("General Setting");
  $scope.name="my data";
  User.get({},function(response){
    $scope.user= response.data;
     // $scope.user.emailalert =$scope.userdetail.emailAlert;
    
  });

  $scope.userName = function(){
    UpdateInfo.change({value: $scope.user.name, type: 'name'}, function(response){
        var confirm = $mdDialog.confirm()
         .title('Successfully Change Name')
         .targetEvent()
         .ok('Okay')
         $mdDialog.show(confirm).then(function() {
          
        }, function() {
          
        });
    }, function(response){
    });
  };

  $scope.checkUserName = function(){
    UpdateInfo.userName({value:$scope.user.slug, type:'name'}, function(response){
      if(response.message == 'already')
      {
        $scope.error=true;
      }
      else
      {
       $scope.error=false; 
      }
    });
  };
  
  $scope.userslug = function(){
    UpdateInfo.change({value:$scope.user.slug, type:'slug'}, function(response){
       var confirm = $mdDialog.confirm()
         .title('Successfully Change Username')
         .targetEvent()
         .ok('Okay')
         $mdDialog.show(confirm).then(function() {
          
        }, function() {
          
        });
    });
  };

  $scope.checkemail = function(){
    UpdateInfo.userName({value:$scope.user.email, type:'email'}, function(response){
      if(response.message == 'already')
      {
        $scope.emailError = true;
      }
      else
      {
       $scope.emailError = false; 
      }
    });
  };
   
  $scope.email = function(){
    UpdateInfo.change({value:$scope.user.email, type:'email'}, function(response){
       var confirm = $mdDialog.confirm()
         .title('Successfully Change Email')
         .targetEvent()
         .ok('Okay')
         $mdDialog.show(confirm).then(function() {
          
        }, function() {
          
        });
    });
  } ;

  $scope.mobileCheck = function(){
    UpdateInfo.userName({value:$scope.user.mobile, type:'mobile'}, function(response){
      if(response.message == 'already')
      {
        $scope.mobileError = true;
      }
      else
      {
        $scope.mobileError = false;
      }
    });
  };

  $scope.mobile = function(){
    UpdateInfo.change({value:$scope.user.mobile, type:'mobile'}, function(response){
      console.log(response.data);
      if(response.data == 'notsame')
      {
        $state.go('info.otp');
      }
    });
  };

  $scope.emailAlert = function(){
    if($scope.user.emailalert == 'true')
    {
      var value = 'Active';
    }else{
      var value = 'Deactive';
    }
    UpdateInfo.change({value:$scope.user.emailalert, type:'alert'}, function(response){
       var confirm = $mdDialog.confirm()
         .title('Successfully '+value+' Email Notification')
         .targetEvent()
         .ok('Okay')
         $mdDialog.show(confirm).then(function() {
          
        }, function() {
          
        });
    });
  }

  $scope.oldPassCheck = function(){
    UpdateInfo.userName({value:$scope.user.oldpassword, type:'password'}, function(response){
      if(response.message =='not')
      {
        $scope.passwordError = true;
      }
      else
      {
       $scope.passwordError = false; 
      }
    });
  };

  $scope.changePassword = function(){
      UpdateInfo.change({oldpassword:$scope.user.oldpassword, newpassword:$scope.user.newpassword, type:'password'}, function(response){
        $scope.user.oldpassword="";
        $scope.user.newpassword="";
        $state.reload();
          var confirm = $mdDialog.confirm()
         .title('Successfully to change Password')
         .targetEvent()
         .ok('Okay')
         $mdDialog.show(confirm).then(function() {
          
        }, function() {
          
        });
      });
  };
  
}])
.controller('locationCtrl',['$scope','Page','$sce', 'User', 'UpdateInfo', '$state','States','Assemblies','Villages','$mdDialog', function($scope,Page,$sce, User, UpdateInfo, $state,States,Assemblies,Villages,$mdDialog){
  Page.setTitle("Location");

function getData(){
  User.get({}, function(resp){
  	resp.$promise.then(function(data){
  		$scope.user = data.data;
  		Assemblies.get({state:data.data.state_id}, function(respo){
  			respo.$promise.then(function(assem){
  				$scope.assemblies = assem.data;
  				Villages.get({type:data.data.assembly_id}, function(vill){
  					vill.$promise.then(function(response){
  						$scope.villages = response.data;
  					});
  				});
  			});
  		});
  	});
  });
};
getData();
  States.get({}, function(response){
    response.$promise.then(function(data){
      $scope.states = data.states;
    });
  });

	$scope.stateChange = function(){
		console.log($scope.user.state_id);
		UpdateInfo.change({value:$scope.user.state_id, type:'state'}, function(response){
			getData();
	       var confirm = $mdDialog.confirm()
	         .title('Successfully Change State')
	         .targetEvent()
	         .ok('Okay')
	         $mdDialog.show(confirm).then(function() {
	          
	        }, function() {
	          
	        });
	    });
	};
  
  $scope.assemblyChange = function(){
		UpdateInfo.change({value:$scope.user.assembly_id, type:'assembly'}, function(response){
			getData();
	       var confirm = $mdDialog.confirm()
	         .title('Successfully Change Assembly')
	         .targetEvent()
	         .ok('Okay')
	         $mdDialog.show(confirm).then(function() {
	        	 
	        }, function() {
	          
	        });
	    });
	};

	$scope.villageChange = function(){
		UpdateInfo.change({value:$scope.user.village_id, type:'village'}, function(response){
			getData();
	       var confirm = $mdDialog.confirm()
	         .title('Successfully Change Assembly')
	         .targetEvent()
	         .ok('Okay')
	         $mdDialog.show(confirm).then(function() {
	        	 
	        }, function() {
	          
	        });
	    });
	};
  

}])
.controller('settingCtrl',['$scope','Page','$sce', function($scope,Page,$sce){
  Page.setTitle("Public Opinion");
  $scope.name="my data";
  
}])
.controller('timelineAssemblyCtrl',['$scope','$rootScope','Page','Posts','addPosts','$q', function($scope,$rootScope,Page,Posts,addPosts,$q){
  Page.setTitle("Assembly timeline");
  $rootScope.selectedTab=2;

  $scope.postsLoading=true;

  $scope.postType="assembly";
  

   // Get the initial posts
  Posts.get({type:$scope.postType, 'offset': 0},function(response){
    $scope.posts= response.data;
    $scope.postsLoading=false;  
  }, function(){
    
  });
  

  $scope.likePost= function(index,id,type){
    $scope.posts[index]= $scope.likeThePost($scope.posts[index],id,type);
  };

  //Load more posts when required
  $scope.loadMorePosts= function(){
    var deferred = $q.defer();


    addPosts.add($scope.postType,typeof $scope.posts == 'undefined' ? [] : $scope.posts).then(function(response){
      $scope.posts= response;
      deferred.resolve(true);
    });

    return deferred.promise;    
  }
}])
.controller('timelinePartyCtrl',['$scope','$rootScope','Page','Posts','addPosts','$q', function($scope,$rootScope,Page,Posts,addPosts,$q){
  
  Page.setTitle("Party timeline");
  $rootScope.selectedTab=1;

  $scope.postsLoading=true;

  $scope.postType="party";

  // Get the initial posts
  Posts.get({type:$scope.postType, 'offset': 0},function(response){
    $scope.posts= response.data;
    $scope.postsLoading=false;
  });

  $scope.likePost= function(index,id,type){

    $scope.posts[index]= $scope.likeThePost($scope.posts[index],id,type);

  };

 
  //Load more posts when required
  $scope.loadMorePosts= function(){

    var deferred = $q.defer();


    addPosts.add($scope.postType,typeof $scope.posts == 'undefined' ? [] : $scope.posts).then(function(response){
      $scope.posts= response;
      deferred.resolve(true);
    });

    return deferred.promise;
    
  }

}])
.controller('timelinePublicCtrl',['$scope','$rootScope','Page','Posts','addPosts','$q', 'urls', '$localStorage', function($scope,$rootScope,Page,Posts,addPosts,$q, urls, $localStorage){
  Page.setTitle("Public timeline");
  $rootScope.selectedTab=0;

  $scope.IMAGE_PATH = urls.IMAGE_PATH;
  

  $scope.postsLoading=false;

  $scope.postType="public";

  $scope.closeComments = function() {

    alert('kokok');
    // $mdDialog.cancel();
  };

  $scope.likePost= function(index,id,type){

    $scope.posts[index]= $scope.likeThePost($scope.posts[index],id,type);

  };

  // Get the initial posts
  Posts.get({type:$scope.postType, 'offset': 0},function(response){
    $scope.posts= response.data;
    $scope.postsLoading=false;
  });


 
  //Load more posts when required
  $scope.loadMorePosts= function(){

    var deferred = $q.defer();



    addPosts.add($scope.postType,typeof $scope.posts == 'undefined' ? [] : $scope.posts).then(function(response){
      $scope.posts= response;
      deferred.resolve(true);
    });

    return deferred.promise;
    
  }



}])
.controller('timelineCtrl',['$scope','Posts','$mdDialog', '$localStorage', '$mdMedia', '$state', '$mdSidenav', function($scope,Posts,$mdDialog, $localStorage, $mdMedia, $state, $mdSidenav){
  $scope.authId = $localStorage.authId;
   
   $scope.rightToggle = function(menuId) {
    $mdSidenav(menuId).toggle();
  };
  
  
  function commentsCtrl($scope,$mdDialog,Posts,currentPost,IMAGE_PATH){

    $scope.post=currentPost;

    $scope.IMAGE_PATH=IMAGE_PATH;


    $scope.updated=0;

    $scope.isLoading= true;

    $scope.comments= new Array();
    

    Posts.comments({id:$scope.post.id},function(response){
      $scope.comments= response.data;

      if(response.data.length == 0)
        $scope.noComments=true;

      $scope.post.comment_count=response.data.length;

      $scope.isLoading= false;



    });


    // console.log($scope.myd);

    $scope.closeComments = function() {

      $mdDialog.cancel();
    };

    $scope.addComment= function(){

      if( (typeof $scope.newComment !== 'undefined' ? $scope.newComment.length : 0 ) > 2){
        Posts.addComment({id:$scope.post.id,comment: $scope.newComment}, function(response){
          $scope.comments.unshift(response.data);

          $scope.updated++;
          $scope.noComments=false;

          $scope.post.comment_count++;

        });

        $scope.newComment="";
      }
    }

  }
  $scope.showComments = function(currentPost) {
    
    $mdDialog.show({
      controller: commentsCtrl,
      templateUrl: 'pages/user/partials/comments.view.html',
      parent: angular.element(document.body),
      // targetEvent: ev,
      clickOutsideToClose:true,      
      locals: {currentPost:currentPost,IMAGE_PATH:$scope.IMAGE_PATH}
    });

  };

  $scope.groupOpen = function(slug)
  {
    $state.go('user.messages.closeGroup', {group:slug});
  }

  $scope.likeThePost= function(post,id,type){
    
    //like
    if(type=='like'){

      //Doesn't like already, gotta like
      if(post.i_like != 1){
        post.like_count++;
        post.i_like=1;
        Posts.like({id:id,type:type});
      }

      post.like_type=1;
      
      return post;
    }

    //dislike

    //Liked earlier, gotta dislike
    if(post.i_like == 1){
      post.i_like=0;
      post.like_count--;
    }

    Posts.like({id:id,type:type});

    post.like_type=0;

    return post;
  };


  $scope.showAlert = function(ev, id) {
   var confirm = $mdDialog.confirm()
   .title('are you sure you want to delete?')
   .targetEvent(ev)
   .ok('Okay')
   .cancel('Cancel');
   $mdDialog.show(confirm).then(function() {
    Posts.delete({id:id}, function(response){
       $state.reload();
    });
  }, function() {
    $scope.status = 'You decided to keep your debt.';
  });
 };


   $scope.profileview = function(slug)
   {
      $mdSidenav(menu)
      .close()
     .then(function(){
       // $log.debug('closed');
     });
      $state.go('user.profile',{ "slug": slug});
   };

   $scope.sharePost = function(postID, userID)
   {
    Posts.share({postId:postID, userId:userID}, function(response){
      $state.reload();
       var confirm = $mdDialog.confirm()
         .title('Successfully Share Repost')
         .targetEvent()
         .ok('Okay')
         $mdDialog.show(confirm).then(function() {
          
        }, function() {
          
        });
    });
   };

   $scope.zoomImage = function(images, size, name)
   {
      $mdDialog.show({
        controller: imageCtrl,
        templateUrl: 'pages/user/partials/zoomImage.view.html',
        parent: angular.element(document.body),
        clickOutsideToClose:false,   
        locals: {currentPost:images, IMAGE_PATH:$scope.IMAGE_PATH, size:size, name:name}   
      });
   };

   function imageCtrl($scope,$mdDialog,currentPost, IMAGE_PATH, size, name){
    $scope.zoomimage = currentPost;
    $scope.IMAGE_PATH = IMAGE_PATH;
    $scope.name = name;
    $scope.size = size;

       $scope.closeImage = function()
       {
          $mdDialog.cancel();
       }

         $scope._Index = 0;
 
    // if a current image is the same as requested image
    $scope.isActive = function (index) {
        return $scope._Index === index;
    };

      // show prev image
    $scope.showPrev = function () {
        $scope._Index = ($scope._Index > 0) ? --$scope._Index : $scope.zoomimage.length - 1;
    };

    
    // show next image
    $scope.showNext = function () {
        $scope._Index = ($scope._Index < $scope.zoomimage.length - 1) ? ++$scope._Index : 0;
    };

    // show a certain image
    $scope.showPhoto = function (index) {
        $scope._Index = index;
    };

   };
 
   
}])












.controller("otpCtrl", ['$scope','Page','$timeout','User','$state','urls', '$http','$localStorage','$mdToast', function($scope, Page, $timeout,User, $state, urls, $http,$localStorage,$mdToast){
	Page.setTitle("Verification");

    User.get({},function(user){
        $scope.user= user.data;  
       if(user.data.verify == '1')
       {
            $state.go('user.timeline.public');
       }
    });


	$scope.counter = 59;
    $scope.onTimeout = function(){
    	if($scope.counter != 0)
        {
            $scope.counter--;
            mytimeout = $timeout($scope.onTimeout,1000);
        }
     };
        var mytimeout = $timeout($scope.onTimeout,1000);

    $scope.submitOTP = function()
    {
        $http.post(urls.API+"/otp/verify/"+$localStorage.authId, $scope.user).then(function(response){
            if(response.data.message == 'success')
            {
                $localStorage.verify=1;
                $state.go('user.timeline.public');
                 $mdToast.show(
                $mdToast.simple()
                    .content('Successfully Submit')
                    .position('bottom left')
                    .hideDelay(3000)
                );
            }
        }, function(error){
            $mdToast.show(
            $mdToast.simple()
                .content('Invalid OTP')
                .position('top left')
                .hideDelay(3000)
            );
        });
    };

    $scope.resend = function()
    {
        $http.get(urls.API+"/otp/resend/"+$localStorage.authId);
        $scope.counter = 59;
        $scope.onTimeout();
    }
}])
.controller('voiceCtrl', ['$scope','Posts','Upload','urls', '$state', function($scope, Posts, Upload, urls, $state){
  $scope.uploadedAudio="";
  $scope.shareButton=false;
  $scope.IMAGE_PATH = urls.IMAGE_PATH;
  // for multiple files:
  $scope.uploadFiles = function (files) {

    if (files && files.length) {
      for (var k = 0; k < files.length; k++) {

        if(typeof $scope.images == 'undefined')
          $scope.images=[];

        $scope.images.push(files[k]);

        files[k].id= $scope.images.length-1;

      }

      for (var i = 0; i < files.length; i++) {

        Upload.upload({
          url: urls.API + '/posts/audio',
          data: {audio: files[i]}

        }).then(function (resp) {
          $scope.uploadedAudio = resp.data.audio;
           if($scope.uploadedAudio != null)
          {
            $scope.shareButton=true;
          }
        }, function (resp) {
        }, function (evt) {
          var progressPercentage= parseInt(100.0 * evt.loaded / evt.total);

          $scope.images[evt.config.data.audio.id]['progress']= progressPercentage;
        });

      }
    }
  };

  $scope.shareAudio = function(id){
  
  if( (typeof $scope.uploadedAudio !== 'undefined' ? $scope.uploadedAudio.length : 0 )){
      Posts.save({audio: $scope.uploadedAudio, type: 'audio'}, function(response){
        $scope.$parent.closeModal();
        $state.reload();
      }, function(response){
      });

      $scope.statusMsg="";

      }
  };

}])

.controller('imagesCtrl', ['$scope','Posts','Upload','urls', '$localStorage', '$state', function($scope,Posts,Upload,urls, $localStorage, $state){
  $scope.uploadedImages=[];
  $scope.shareButton=false;

  // for multiple files:
    $scope.uploadFiles = function (files) {
      if (files && files.length) {
        for (var k = 0; k < files.length; k++) {

          if(typeof $scope.images == 'undefined')
            $scope.images=[];

          $scope.images.push(files[k]);

          files[k].id= $scope.images.length-1;
     
        }

        for (var i = 0; i < files.length; i++) {

          Upload.upload({
            url: urls.API + '/posts/image',
            data: {image: files[i]}
        }).then(function (resp) {
          $scope.uploadedImages.push(resp.data.image);
          if($scope.uploadedImages != null)
          {
            $scope.shareButton=true;
          }

        }, function (resp) {
        }, function (evt) {
          var progressPercentage= parseInt(100.0 * evt.loaded / evt.total);

          $scope.images[evt.config.data.image.id]['progress']= progressPercentage;
        });

     
        }
      }
    };
  
$scope.shareImage = function(){

  if( (typeof $scope.uploadedImages !== 'undefined' ? $scope.uploadedImages.length : 0 )){
      Posts.save({images: angular.fromJson(angular.toJson($scope.uploadedImages)), type: 'images'}, function(response){
        $scope.$parent.closeModal();
        $state.reload();
      }, function(response){
      });

      $scope.statusMsg="";

      }
  };

}])
.controller('statusCtrl', ['$scope','Posts', '$state', function($scope,Posts, $state){

  $scope.addStatus= function(){

  	if( (typeof $scope.statusMsg !== 'undefined' ? $scope.statusMsg.length : 0 ) > 2){
      Posts.save({status: $scope.statusMsg, type: 'text'}, function(response){
        $scope.$parent.closeModal();
        $state.reload();
      }, function(response){
      });

      $scope.statusMsg="";

      }
  };

}])
.controller('videoCtrl', ['$scope','Posts','Upload','urls', '$localStorage', '$state', function($scope,Posts,Upload,urls, $localStorage, $state){
$scope.shareButton=false;
  $scope.uploadedVideo="";
  $scope.IMAGE_PATH = urls.IMAGE_PATH;
  // for multiple files:
    $scope.uploadFiles = function (files) {
      if (files && files.length) {
        for (var k = 0; k < files.length; k++) {

          if(typeof $scope.images == 'undefined')
            $scope.images=[];

          $scope.images.push(files[k]);

          files[k].id= $scope.images.length-1;
     
        }

        for (var i = 0; i < files.length; i++) {

          Upload.upload({
            url: urls.API + '/posts/video',
            data: {video: files[i]}
        }).then(function (resp) {
          $scope.uploadedVideo = resp.data.video;
           if($scope.uploadedVideo != null)
          {
            $scope.shareButton=true;
          }
        }, function (resp) {
        }, function (evt) {
          var progressPercentage= parseInt(100.0 * evt.loaded / evt.total);

          $scope.images[evt.config.data.video.id]['progress']= progressPercentage;
        });

     
        }
      }
    };
  
$scope.shareVideo = function(){
  if( (typeof $scope.uploadedVideo !== 'undefined' ? $scope.uploadedVideo.length : 0 )){
      Posts.save({video: $scope.uploadedVideo, type: 'video'}, function(response){
        $scope.$parent.closeModal();
        $state.reload();
      }, function(response){
      });

      $scope.statusMsg="";

      }
  };

}])
.controller('infoHeaderCtrl', ['$scope','Auth', function($scope, Auth){

	$scope.logout= function(){
    	Auth.logout();
  	}
}])
/*** Home controller ***/
.controller('homeCtrl', ['$scope', function($scope){



  // $scope.isLoggedIn= function(){
  //   return Auth.loggedIn();
  // }

}])

/*** Guest controller main ***/
.controller('guestCtrl', ['$scope','Auth', function($scope,Auth){

	  if(Auth.loggedIn())
    	$state.go('user.timeline.public');
}])



/*** User controller ***/
.controller('userCtrl', ['$scope','Page','Auth','$timeout','$http','urls','$interval','$localStorage','User','$state', 'chatSocket', '$mdSidenav', function($scope,Page,Auth,$timeout,$http,urls,$interval,$localStorage,User,$state, chatSocket, $mdSidenav){
 
//   document.onkeydown = function(){
//   switch (event.keyCode){
//         case 116 : //F5 button
//             event.returnValue = false;
//             event.keyCode = 0;
//             return false;
//         case 82 : //R button
//             if (event.ctrlKey){ 
//                 event.returnValue = false;
//                 event.keyCode = 0;
//                 return false;
//             }
//     }
// }

  
  Page.setTitle("Timeline");

  if(!Auth.loggedIn())
    $state.go('guest.default');


  // User.get({},function(user){
  //   $scope.user= user.data;
  //   console.log(user.data, 'user data');
  //   chatSocket.emit('onlineUser',{id:$localStorage.authId, name:user.data.name, avatar:user.data.avatar});
  // });

  // Auth.online();



 $scope.profileview = function(slug, menu)
   {
    $mdSidenav(menu).close().then(function(){
      // $log.debug('closed');
    });
      $state.go('user.profile',{ "slug": slug});
      
   };
  // $http.post(urls.API + '/data').success(function(data){
  //   $scope.data1=JSON.stringify(data.data);
  //   console.log($scope.data1);
  // });

  // $http.post(urls.API + '/data').success(function(data){
  //   $scope.data2=JSON.stringify(data.data);
  //   console.log($scope.data2);
  // });
  $interval(offline, 500000)
  function offline(){
    
    Auth.offline();
  }

  $interval(callAtTimeout, 1000000);

  function callAtTimeout() {

    Auth.refreshToken(refreshSuccess);
    

  }

  function refreshSuccess(data){
    if(data.newToken)
      $localStorage.token = data.newToken;
  }


$scope.emoji = [
  ":bowtie:",
  ":smile:",
  ":simple_smile:"
];

  
}])













